// fill_dl.js
// Requires scripts jQuery + Bootstrap
// Requires variables stable_ver, oldstable_ver, development_ver

var fill_dl_nitialized = false;
//var dl_arrow = '<img src="/image/dlarrow.png">';
//var dl_arrow = '<i class="glyphicon glyphicon-download"></i>';
var dl_arrow = '<i class="fa fa-download"></i>';
var total_weight = 0;
var rand_weight = 0;
var cur_weight = 0;

function weighted_random_location() {

  for (i = 0; i < locations.length; i++) {
    total_weight += locations[i]['weight'];
  }
  rand_weight = Math.floor(total_weight * Math.random());

  for (i = 0; i < locations.length; i++) {
    cur_weight += locations[i]['weight'];

    if (cur_weight > rand_weight) {
      break;
    }
  }

  return locations[i];
}

var wr_loc = weighted_random_location();

function name_sort(a, b) {
    return ( a.name.toLowerCase() > b.name.toLowerCase() );
}

var platform = 'unknown';
// Figure out the client platform. MUST be called before other functions.
function init_fill_dl() {
  if (fill_dl_nitialized) return;

  if (typeof navigator == 'object') {
    if (typeof navigator.userAgentData == 'object') {
      platform = navigator.userAgentData.platform;
    } else {
      platform = navigator.platform;
    }
  }
  platform = platform.toLowerCase();

  // Assume that all of our Windows users are 64-bit.
  if (platform == 'win32' || platform == 'windows') platform = 'win64';
  fill_dl_nitialized = true;
}

function update_dl_elements(selector, url_pfx) {
  init_fill_dl();

  var re = new RegExp(url_pfx, 'gm');
  var els = document.querySelectorAll(selector);
  Array.prototype.forEach.call(els, function(el) {
    el.innerHTML = el.innerHTML.replace(re, wr_loc['url']);

    var platform_selector = ".dl_arrow.platform-" + platform;
    document.querySelector(platform_selector).innerHTML = dl_arrow;
  });
}

function fill_dl_mirrors() {
  init_fill_dl();

  var mirr_html = '<ul class="item-list">\n';
  locations.sort(name_sort);
  Array.prototype.forEach.call(locations, function(loc) {
    mirr_html += '<li><a href="' + locations[loc].url + '">' + locations[loc].name + '</a></li>';
  });
  mirr_html += '</ul>\n';
  document.querySelectorAll("#dl_mirrors").innerHTML = mirr_html;
}
