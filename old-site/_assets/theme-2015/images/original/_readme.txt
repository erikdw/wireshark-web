The original copies of any images that have had lossy compression
applied should be placed here.

E.g. many large PNGs in the parent directory have been compressed using
pngquant: https://pngquant.org/
