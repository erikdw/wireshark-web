#!/bin/bash
#
# build-dist - Build the main web site

set -e -u -o pipefail

TOP_LEVEL=$( git rev-parse --show-toplevel )
cd "$TOP_LEVEL"

printf "Working around https://github.com/withastro/astro/issues/6167\n"
while IFS= read -r idx_a ; do
    mv -v "$idx_a" "${idx_a/index.astro/default.astro}"
done < <(find src/pages -mindepth 2 -name index.astro)

yarn run astro build

printf "Restoring source index files\n"
while IFS= read -r def_a ; do
    mv -v "$def_a" "${def_a/default.astro/index.astro}"
done < <(find src/pages -mindepth 2 -name default.astro)

printf "Renaming dist index files\n"
while IFS= read -r def_f ; do
    mv -v "$def_f" "${def_f/default.html/index.html}"
done < <(find dist -name default.html)
