#!/bin/bash

set -e

if ! type -p yq > /dev/null 2>&1 ;then
    echo "yq command not found"
fi

ADVISORIES=$@

if [ -z "$ADVISORIES" ] ; then
    ADVISORIES=$( ls wnpa-sec-"$( date +%Y)"-??.yml )
fi

for ADVISORY in $ADVISORIES ; do
    printf "https://www.wireshark.org/security/%s.html\\n" "$( basename "$ADVISORY" .yml )"
    ISSUE_ID=$( grep ^bug_ids "$ADVISORY" | sed -e "s/^bug_ids.*\['//" -e "s/'\]//" )
    if [ -n "$ISSUE_ID" ] ; then
        printf "https://gitlab.com/wireshark/wireshark/-/issues/%d\\n" "$ISSUE_ID"
    fi

    printf "\\n"
done