#!/usr/bin/env python3
'''\
gen-appcasts.py - Generate signed Sparkle Appcasts for the latest release.
'''

#
# Imports
#

import argparse
import configparser
import hashlib
import importlib
import json
import os.path
import re
import subprocess
import sys
import tempfile
import urllib.request

from enum import Enum
from string import Template

#
# Globals
#

dl_pfx = 'https://www.wireshark.org/download'
version_info = importlib.import_module("get-version-info").version_info
# https://github.com/sparkle-project/Sparkle/blob/2.x/Resources/SampleAppcast.xml
macos_tmpl = Template('''\
<?xml version="1.0" encoding="utf-8"?>
<rss xmlns:sparkle="http://www.andymatuschak.org/xml-namespaces/sparkle" version="2.0">
<channel>
  <title>Wireshark ${channel_title} Release</title>
  <link>${link}</link>
  <description>The latest ${channel} release of Wireshark.</description>
  <item>
    <title>Version ${version}</title>
    <sparkle:minimumSystemVersion>${min_system_version}</sparkle:minimumSystemVersion>
    <pubDate>${pub_date}</pubDate>
    <sparkle:releaseNotesLink>https://www.wireshark.org/update/relnotes/wireshark-${version}.html</sparkle:releaseNotesLink>
    <enclosure
${enclosure}
      type="application/octet-stream"/>
      <!-- SHA256: ${sha256} -->
  </item>
</channel>
</rss>
''')
windows_tmpl = Template('''\
<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xmlns:sparkle="http://www.andymatuschak.org/xml-namespaces/sparkle">
<channel>
  <title>Wireshark ${channel_title} Release</title>
  <link>${link}</link>
  <description>The latest ${channel} release of Wireshark.</description>
  <language>en</language>
  <item>
    <title>Version ${version}</title>
    <sparkle:releaseNotesLink>https://www.wireshark.org/update/relnotes/wireshark-${version}.html</sparkle:releaseNotesLink>
    <pubDate>${pub_date}</pubDate>
    <enclosure
${enclosure}
      type="application/octet-stream">
    </enclosure>
  </item>
</channel>
</rss>
''')
architectures = (
    'wireshark-macos-arm64',
    'wireshark-macos-x86-64',
    'wireshark-windows-arm64',
    'wireshark-windows-x86',
    'wireshark-windows-x86-64',
)


#
# Classes
#

class Channel(Enum):
    STABLE = 1
    DEVELOPMENT = 2

#
# Functions
#

def main():
    channel = 'stable'
    top_level = os.path.normpath(os.path.join(os.path.dirname(__file__), '..'))
    signatures_path = os.path.join(top_level, 'data', 'sparkle-signatures.json')
    component_path = os.path.join(top_level, 'src', 'components', 'Download')

    parser = argparse.ArgumentParser(description='Generate Sparkle signatures.')
    parser.add_argument('-d', '--development', action='store_true', help='Sign the development version.')
    args = parser.parse_args()

    if args.development:
        channel = 'development'

    dmg_version = version_info[channel]

    with open(os.path.join(component_path, 'Versions.json'), 'r') as key_f:
        versions = json.load(key_f)

    # Update sparkle-signatures.json
    with open(signatures_path, 'r') as sig_f:
        signatures = json.load(sig_f)
    signatures['generator'] = os.path.basename(__file__)

    macos_architectures = ('Arm', 'Intel')
    # macos_architectures = ()
    for arch in macos_architectures:
        if arch == 'Arm':
            sparkle_arch = 'wireshark-macos-arm64'
        else:
            sparkle_arch = 'wireshark-macos-x86-64'

        # signatures[sparkle_arch][channel]['eddsa'] = input(f'Enter the Sparkle signature for {dmg}: ')
        # signatures[sparkle_arch][channel]['sha256'] = input(f'Enter the SHA256 for {dmg}: ')

        relinfo_url = f'{dl_pfx}/automated/.release/release-info-{dmg_version}-macos-{arch.lower()}64.ini'
        with urllib.request.urlopen(relinfo_url) as response:
            relinfo = configparser.ConfigParser()
            relinfo.read_string(response.read().decode('utf-8'))

            signatures[sparkle_arch][channel]['eddsa'] = relinfo['DEFAULT']['sparkle_signature']
            signatures[sparkle_arch][channel]['sha256'] = relinfo['DEFAULT']['dmg_sha256']

        # dmg_url = '{}/osx/{}'.format(dl_pfx, dmg)
        # dmg_sha256 = hashlib.sha256()
        # with urllib.request.urlopen(dmg_url) as response:
        #     with tempfile.NamedTemporaryFile(prefix=dmg+'.', delete=False) as dmg_tmp:
        #         print('Downloading {}\n  to {}'.format(dmg_url, dmg_tmp.name))
        #         while True:
        #             dmg_data = response.read(2**20)
        #             if not dmg_data: break
        #             dmg_sha256.update(dmg_data)
        #             dmg_tmp.write(dmg_data)

        #         dmg_tmp.flush()
        #         su_cmd = subprocess.run(['sign_update', dmg_tmp.name],
        #             capture_output=True,
        #             encoding='UTF-8'
        #             )
        #         signatures[sparkle_arch][channel]['eddsa'] = su_cmd.stdout.strip()
        #         signatures[sparkle_arch][channel]['sha256'] = dmg_sha256.hexdigest()

        dmg = 'Wireshark%20{}%20{}%2064.dmg'.format(dmg_version, arch)
        print(f'''
{dmg}:
  Signature : {signatures[sparkle_arch][channel]['eddsa']}
  SHA256    : {signatures[sparkle_arch][channel]['sha256']}
''')

    # XXX We should update this automatically.
    signatures['wireshark-macos-arm64']['stable']['min_system_version'] = '11.0'
    signatures['wireshark-macos-x86-64']['stable']['min_system_version'] = '10.13'
    signatures['wireshark-macos-arm64']['development']['min_system_version'] = '11.0'
    signatures['wireshark-macos-x86-64']['development']['min_system_version'] = '11.0'

    with open(signatures_path, 'w') as sig_f:
        sig_f.write(json.dumps(signatures, indent=2))
    print('Updated sparkle-signatures.json\n')

    appcast_init = {
        Channel.STABLE: {
            'channel': 'stable',
            'channel_title': 'Stable',
            'link': 'https://www.wireshark.org/download.html',
        },
        Channel.DEVELOPMENT: {
            'channel': 'development',
            'channel_title': 'Development',
            'link': 'https://www.wireshark.org/development.html',
        },
    }

    for sparkle_arch in architectures:
        is_oldstable = sparkle_arch == 'wireshark-windows-x86'
        stable_info = appcast_init[Channel.STABLE].copy()
        old_pfx = ''
        if is_oldstable:
            old_pfx = 'old'
        stable_info.update({
            'version': versions[old_pfx + 'stable'],
        })
        stable_info.update(signatures[sparkle_arch]['stable'])
        stable_info.update(versions[sparkle_arch][old_pfx + 'stable'])

        if 'macos' in sparkle_arch:
            appcast_tmpl = macos_tmpl
            stable_info['enclosure'] = f'''\
      url="{versions["download_prefix"]}/{stable_info["download_path"]}"
      sparkle:version="{versions["stable"]}" sparkle:shortVersionString="{versions["stable"]}"
      {stable_info["eddsa"]}\
'''
        else:
            try:
                appcast_tmpl = windows_tmpl
                version = versions[old_pfx + 'stable']
                stable_info['enclosure'] = f'''\
        url="{versions["download_prefix"]}/{stable_info["download_path"]}"
        sparkle:version="{version}" sparkle:shortVersionString="{version}"
        length="{stable_info["bytes"]}"\
'''
            except KeyError:
                print(f'= Unable to create stable info enclosure for {sparkle_arch}')

        if is_oldstable:
            development_info = stable_info.copy()
        elif versions['development'] is None:
            development_info = stable_info.copy()
            development_info.update(appcast_init[Channel.DEVELOPMENT])
        else:
            development_info = appcast_init[Channel.DEVELOPMENT].copy()
            development_info.update({
                'version': versions['development'],
            })
            development_info.update(signatures[sparkle_arch]['development'])
            development_info.update(versions[sparkle_arch]['development'])

            if 'macos' in sparkle_arch:
                development_info['enclosure'] = f'''\
      url="{versions["download_prefix"]}/{development_info["download_path"]}"
      sparkle:version="{versions["development"]}" sparkle:shortVersionString="{versions["development"]}"
      {development_info["eddsa"]}\
'''
            else:
                appcast_tmpl = windows_tmpl
                development_info['enclosure'] = f'''\
        url="{versions["download_prefix"]}/{development_info["download_path"]}"
        sparkle:version="{versions["development"]}" sparkle:shortVersionString="{versions["development"]}"\
'''

        try:
            with open(os.path.join(top_level, 'update', 'staging', sparkle_arch, f'stable.xml'), 'w') as appcast_f:
                appcast_f.write(appcast_tmpl.substitute(stable_info))
        except:
            print(f'= Skipping {sparkle_arch}/stable.xml')

        with open(os.path.join(top_level, 'update', 'staging', sparkle_arch, f'development.xml'), 'w') as appcast_f:
            appcast_f.write(appcast_tmpl.substitute(development_info))

    return 0

#
# On with the show
#

if __name__ == "__main__":
    sys.exit(main())
