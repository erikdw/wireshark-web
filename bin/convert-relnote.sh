#!/bin/bash
#
# Convert-relnote - convert an HTML release note generated by xsltproc
# to Astro.js.

set -e -u -o pipefail

BASEDIR="$( realpath "$( dirname "$0" )" )"
UPDATEDIR="$( realpath "$BASEDIR/../update/relnotes" )"
BUILD_CMD=ninja
TOP_LEVEL=$( git rev-parse --show-toplevel )
if [ -f "${TOP_LEVEL}"/doc/release-notes.adoc ] ; then
    RELNOTE="$( realpath "${TOP_LEVEL}"/build/doc/release-notes.html )"
else
    RELNOTE="$( realpath "${TOP_LEVEL}"/build/docbook/release-notes.html )"
fi

cd "${TOP_LEVEL}/build" || exit 1

rm -f doc*/release-notes.{html,txt}
$BUILD_CMD release_notes_html
$BUILD_CMD release_notes_txt
$BUILD_CMD news

if [ ! -s "$RELNOTE" ] ; then
    echo "Can't find $RELNOTE (or it's empty)"
    exit 1
fi

VERSION=$(\
    xmllint --html --format --xpath '/html/head/title/text()' "$RELNOTE" | \
    head -n 1 | \
    sed -E -e 's:^.*Wireshark ([0-9.]+) Release Notes.*$:\1:' \
    )
if [ -z "$VERSION" ] ; then
    echo "Can't find version"
    exit 1
fi

VER_FILE="wireshark-$VERSION.html"

echo "Copying $RELNOTE to $UPDATEDIR/$VER_FILE"

#cp $RELNOTE $UPDATEDIR/$VER_FILE || exit 1

# Nice things are always just out of reach.
# https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=16337
# https://github.com/sparkle-project/Sparkle/issues/430
# https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=16091
# https://github.com/vslavik/winsparkle/issues/177

# sed -e 's:content="IE=.*">:content="IE=EmulateIE9">:' \
#     < $RELNOTE \
#     | gsed '/<\/head>/i <!--[if IE]><base target="_blank"><![endif]-->' \
#     > "$UPDATEDIR/$VER_FILE" || exit 1

WHATS_NEW=$(xmllint \
        --html \
        --format \
        --xpath '/html/body[@class="article"]/div[@id="content"]/div[@class="sect1"]/h2[@id="_whats_new"]/..' \
        "$RELNOTE" \
        | sed -e 's/ *target="_top"//g' \
)

cat > "$UPDATEDIR/$VER_FILE" <<FIN
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="generator" content="Asciidoctor 2.0.18">
<title>Wireshark $VERSION Release Notes</title>
<link rel="stylesheet" href="./ws.css">
<!--[if IE]><base target="_blank"><![endif]-->
</head>
<body class="article">

$WHATS_NEW

</body>
</html>
FIN

( cd "$UPDATEDIR" && git add "$VER_FILE" )

echo "Converting $RELNOTE to $VER_FILE"

cd "$BASEDIR/../src/pages/docs/relnotes/_src"

# Asciidoctor: /html/body[@class="article"]
xmllint \
    --html \
    --format \
    --xpath '/html/body[@class="article"]/*' \
    "$RELNOTE" \
    | sed -e 's/ *target="_top"//g' \
    > "$VER_FILE"

grep 'target="_top"' -- *.html > /dev/null && echo "Remove _top from these files:"
grep -l 'target="_top"' -- *.html

"$BASEDIR/create-relnote-list.py"

git add "../$VER_FILE"
