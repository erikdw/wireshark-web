#!/usr/bin/env python3
'''\
get-version-info - Generate Jinja2 macros for version numbers, dates, and sizes.
'''

#
# Imports
#

import json
import os.path
import re
import sys
import time
import traceback
import urllib.request, urllib.error, urllib.parse
from email.utils import parsedate_tz

#
# Global variables
#

top_dir = os.path.normpath(os.path.join(os.path.dirname(__file__), '..'))
url_pfx = 'https://www.wireshark.org/download/'
version_info = {
    'generator':   os.path.basename(__file__),
    'stable':           '4.0.10',
    'oldstable':        '3.6.18',
    #'development':      None,
    'development':      '4.2.0rc1',
    # 'stable_log':       '0.0.0',
    # 'oldstable_log':    '0.0.0',
    # 'development_log':  '0.0.0',
    # The default prefix for each of the downloads below. fill_dl.js
    # will replace this with the nearest mirror.
    'download_prefix':  'https://2.na.dl.wireshark.org',
    'wireshark-macos-arm64': {
        'stable': { 'pub_date': None },
        'oldstable': { 'pub_date': None },
        'development': { 'pub_date': None }
    },
    'wireshark-macos-x86-64': {
        'stable': { 'pub_date': None },
        'oldstable': { 'pub_date': None },
        'development': { 'pub_date': None }
    },
    'wireshark-windows-arm64': {
        'stable': { 'pub_date': None },
        'oldstable': { 'pub_date': None },
        'development': { 'pub_date': None }
    },
    'wireshark-windows-x86': {
        'stable': { 'pub_date': None },
        'oldstable': { 'pub_date': None },
        'development': { 'pub_date': None }
    },
    'wireshark-windows-x86-64': {
        'stable': { 'pub_date': None },
        'oldstable': { 'pub_date': None },
        'development': { 'pub_date': None }
    },
}

#
# Functions
#

def get_installer_info(dl_dir, installer):
    download_path = f'{dl_dir}/{installer}'
    req = urllib.request.Request(f'{url_pfx}{download_path}')
    req.get_method = lambda : 'HEAD'
    resp = urllib.request.urlopen(req)
    headers = dict(resp.info())
    content_length = int(headers['Content-Length'])
    last_modified = headers['Last-Modified']
    print(f'Got {last_modified}, {content_length / 1024 / 1024:.0f} MiB for {urllib.parse.unquote(installer)}')
    return {
        'bytes': content_length,
        'download_path': download_path,
        'pub_date': last_modified,
    }


def update_pad_xml():
    '''wireshark-pad.xml is used by our monitoring system to determine the current stable version number.'''
    lines = ''
    pub_time = parsedate_tz(version_info['wireshark-windows-x86-64']['stable']['pub_date'])
    pub_year = pub_time[0]
    pub_month = pub_time[1]
    pub_day = pub_time[2]
    size_bytes = version_info['wireshark-windows-x86-64']['stable']['bytes']
    download_file = os.path.split(version_info['wireshark-windows-x86-64']['stable']['download_path'])[1]
    substitutions = {
        'Program_Version': version_info['stable'],
        'Program_Release_Year': pub_year,
        'Program_Release_Month': pub_month,
        'Program_Release_Day': pub_day,
        'File_Size_Bytes': size_bytes,
        'File_Size_K': f'{size_bytes / 1024:.2f}',
        'File_Size_MB': f'{size_bytes / 1024 / 1024:.2f}',
        'Primary_Download_URL': 'https://www.wireshark.org/download/win64/all-versions/' + download_file,
        'Secondary_Download_URL': 'https://1.na.dl.wireshark.org/win64/all-versions/' + download_file,
        'Additional_Download_URL_1': 'https://2.na.dl.wireshark.org/win64/all-versions/' + download_file,
    }

    with open(os.path.join(top_dir, 'public', 'wireshark-pad.xml'), 'r') as pad_xml_f:
        for line in pad_xml_f:
            for tag, value in substitutions.items():
                pattern = rf'(?P<open>.*?<{tag}>).+?(?P<close></{tag}>.*)'
                repl = rf'\g<open>{value}\g<close>'
                line = re.sub(pattern, repl, line)
            lines += line

    with open(os.path.join(top_dir, 'public', 'wireshark-pad.xml'), 'w') as pad_xml_f:
        pad_xml_f.write(lines)
        print('\nUpdated wireshark-pad.xml')


def get_dfilter_list(version):
    dfilter_file = f'dfilter-list-{version}.txt'
    req = urllib.request.Request(f'{url_pfx}automated/.release/{dfilter_file}')
    req.get_method = lambda : 'HEAD'
    resp = urllib.request.urlopen(req)
    dfilter_lines = resp.read().decode('utf-8')

    with open(os.path.join(top_dir, 'data/dfilter-db', dfilter_file), 'w') as dfilter_f:
        dfilter_f.write(dfilter_lines)
        print(f'\nFetched {dfilter_file}')


# The server returns GMT dates. We might want to add a local-fudged time value.
def main():
    try:
        installer = f"Wireshark-win64-{version_info['stable']}.exe"
        version_info['wireshark-windows-x86-64']['stable'].update(get_installer_info('win64', installer))

        installer = f"Wireshark%20{version_info['stable']}%20Intel%2064.dmg"
        version_info['wireshark-macos-x86-64']['stable'].update(get_installer_info('osx', installer))

        installer = f"Wireshark%20{version_info['stable']}%20Arm%2064.dmg"
        version_info['wireshark-macos-arm64']['stable'].update(get_installer_info('osx', installer))

        installer = f"Wireshark-win64-{version_info['oldstable']}.exe"
        version_info['wireshark-windows-x86-64']['oldstable'].update(get_installer_info('win64', installer))

        installer = f"Wireshark%20{version_info['oldstable']}%20Intel%2064.dmg"
        version_info['wireshark-macos-x86-64']['oldstable'].update(get_installer_info('osx', installer))

        installer = f"Wireshark%20{version_info['oldstable']}%20Arm%2064.dmg"
        version_info['wireshark-macos-arm64']['oldstable'].update(get_installer_info('osx', installer))

        try:
            installer = f"Wireshark-win32-{version_info['oldstable']}.exe"
            version_info['wireshark-windows-x86']['oldstable'].update(get_installer_info('win32', installer))
        except (TypeError, urllib.error.HTTPError):
            print(('= No date for Win32 oldstable (' + repr(version_info['oldstable']) + ')'))

        try:
            installer = f"Wireshark-{version_info['development']}-x64.exe"
            version_info['wireshark-windows-x86-64']['development'].update(get_installer_info('win64', installer))
        except (TypeError, urllib.error.HTTPError):
            print(('= No date for Windows x64 development (' + repr(version_info['development']) + ')'))

        try:
            installer = f"Wireshark-{version_info['development']}-arm64.exe"
            version_info['wireshark-windows-arm64']['development'].update(get_installer_info('win64', installer))
        except (TypeError, urllib.error.HTTPError):
            print(('= No date for Windows arm64 development (' + repr(version_info['development']) + ')'))

        try:
            installer = f"Wireshark%20{version_info['development']}%20Intel%2064.dmg"
            version_info['wireshark-macos-x86-64']['development'].update(get_installer_info('osx', installer))
        except (TypeError, urllib.error.HTTPError):
            print(('= No date for macOS Intel development (' + repr(version_info['development']) + ')'))

        try:
            installer = f"Wireshark%20{version_info['development']}%20Arm%2064.dmg"
            version_info['wireshark-macos-arm64']['development'].update(get_installer_info('osx', installer))
        except (TypeError, urllib.error.HTTPError):
            print(('= No date for macOS Arm development (' + repr(version_info['development']) + ')'))

    except:
        traceback.print_exc()
        return 1

    version_path = os.path.join(os.path.dirname(__file__), '..', 'src', 'components', 'Download', 'Versions.json')
    with open (version_path, 'w') as vj:
        json.dump(version_info, vj, sort_keys=True, indent=4)
        print('\nUpdated Versions.json')

    update_pad_xml()

    get_dfilter_list(version_info['stable'])
    get_dfilter_list(version_info['oldstable'])

    return 0

#
# On with the show
#

if __name__ == "__main__":
    sys.exit(main())
