#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# get-authors.py
#
# Given the path to the AUTHORS file in the Wireshark distribution,
# generate a list of authors suitable for inclusion in about.html.
#

#
# Imports
#

import html
import os.path
import sys
import re
import urllib.request, urllib.parse, urllib.error

#
# Global variables
#

git_url_tmpl = u'https://gitlab.com/wireshark/wireshark/-/raw/{}/AUTHORS'
version = u'master'

#
# Functions
#

def try_url(url):
    authors = []	# Too bad dictionaries don't preserve sequence order
    pat = '(.+)\s+<(.+\[AT].+)>'
    req_headers = { 'User-Agent': 'Wireshark get-authors' }
    req = urllib.request.Request(url, headers=req_headers)
    authfile = urllib.request.urlopen(req)
    for line in authfile.readlines():
       m = re.match(pat, line.decode('utf-8'), flags=re.IGNORECASE)
       if m != None:
            name = m.group(1).strip()
            if name == '':
                name = '<i>Name unknown</i>'
            authors.append((name, m.group(2).strip()))
    authfile.close()
    return authors

#
# On with the show
#

if len(sys.argv) < 1:
    version = sys.argv[1]

if version.startswith('master'):
    authors = try_url(git_url_tmpl.format(version))

if len(authors) < 300:
    authors = try_url(git_url_tmpl.format('master-' + sys.argv[1]))

if len(authors) < 300:
    print('''\

Usage: %s <version>

Where <version> is 'trunk' or a valid wireshark version under
%s
    ''' % (sys.argv[0], git_url_tmpl), file=sys.stderr)
    sys.exit(1)

fmt_str = '    <tr><td>%s</td><td>&lt;%s&gt;</td></tr>\n'

authors_path = os.path.join(os.path.dirname(__file__), '..', 'src', 'components', 'AboutPage', 'AuthorList.astro')
with open(authors_path, 'w') as authors_f:
    authors_f.write('''\
---
// Author list
// Do not edit
// Automatically generated using %(generator)s
---

<table id="authorList">
<tr><th colspan="2">Original Author</th></tr>
''' % { 'generator' : os.path.basename(__file__) } )

    name, addr = authors[0]
    authors_f.write(fmt_str % (name, addr))

    authors_f.write(''' \
<tr><th colspan="2">Contributors</th></tr>
''')

    for name, addr in authors[1:]:
        name = html.escape(name)
        addr = html.escape(addr)
        authors_f.write(fmt_str % (name, addr))

    authors_f.write('</table>\n')
