#!/usr/bin/env python3
'''\
get-blog-entries - Generates a Jinja2 macro listing recent blog entries.
'''

import feedparser
import os.path
import sys
import time
import traceback

#sys.path.append('/web/staging.wireshark.org/wsweb/js/')
sys.path.append('/home/gerald/bin')


feed_url = 'https://blog.wireshark.org/feed/'

cat_img_map = {
    'Announcement': 'announcement',
    'Humor': 'entertainment',
    'Info': 'info',
    'Infrastructure': 'infrastructure',
    'Analysis': 'investigate',
    'Protocols': 'protocols',
    'Tip': 'tip',
    'Tools': 'tools',
    'Video': 'video',
}

cat_img_default = 'default'

def main(minimize=True):
    content = u'''\
{#
    Blog entries
    = DO NOT EDIT =
    Automatically generated using %(generator)s
    $Id$
#}
{%% macro list() %%}
''' % { 'generator' : os.path.basename(__file__) }
    fdata = {}
    num_entries = 0
    try:
        feed = feedparser.parse(feed_url)
        num_entries = min(3, len(feed['entries']))
        entry_l = list(range(0, num_entries))


        for i in entry_l:
            cfld = feed['entries'][i]

            cfld['WS_cat_img'] = cat_img_default

            for tag in feed['entries'][i].tags:
                term = tag['term']
                if term in cat_img_map:
                    cfld['WS_cat_img'] = cat_img_map[term]
                    break

            cfld['WS_date'] = time.strftime("%b %e", cfld.updated_parsed)
            if i == entry_l[-1]:
                cfld['WS_last'] = ' last'
            else:
                cfld['WS_last'] = ''

            content += '''\
          <div class="record%(WS_last)s">
            <h2 class="title"><a href="%(link)s" title="%(title)s"><div class="pull-right blog-category blog-category-%(WS_cat_img)s"></div>%(title)s</a></h2>
            <p class="postinfo">%(WS_date)s &#124; By %(author)s</p>
          </div>
'''         % cfld

        cfld = feed['feed']
        if 'link' in feed:
            content += '''\
              <p><a class="more" href="%(link)s" title="%(title)s&#8230;"><b>%(title)s Blog <span class="link-arrow"></span></b></a></p>
    '''         % cfld

    except:
        traceback.print_exc()
        return 1

    content += '''\
{% endmacro %} ## list
'''
    sys.stdout.write(content)
    return 0

if __name__ == "__main__":
    sys.exit(main())
