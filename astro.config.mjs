import { defineConfig } from 'astro/config';
import preact from "@astrojs/preact";

import partytown from "@astrojs/partytown";

// https://astro.build/config
export default defineConfig({
  // We want each of our .astro files to have a correspondingly-named .html file.
  // For example:
  //   /index.astro → /index.html
  //   /faq.astro → /faq.html
  //   /docs/index.astro → /docs/index.html.
  // This appears to be impossible with Astro. The Astro docs say
  //
  // "Control the output file format of each page.
  //   If ‘file’, Astro will generate an HTML file (ex: “/foo.html”) for each page.
  //   If ‘directory’, Astro will generate a directory with a nested index.html file (ex: “/foo/index.html”) for each page."
  //
  // They *don't* say "‘file’ will force /foo/index.astro to /foo.html whether you like it or not",
  // but that's what Astro does: https://github.com/withastro/astro/issues/6167
  // For now, name each of subdirectory index files `default.astro` and rename them after build.
  build: {
    format: 'file'
  },
  // Make sure we don't confuse ourselves during development.
  trailingSlash: 'ignore',
  integrations: [preact(), partytown()]
});