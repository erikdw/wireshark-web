// update_mirror_url.js

function weighted_random_url() {
  var total_weight = 0;
  var rand_weight = 0;
  var cur_weight = 0;
  
  for (i = 0; i < locations.length; i++) {
    total_weight += locations[i]['weight'];
  }
  rand_weight = Math.floor(total_weight * Math.random());

  for (i = 0; i < locations.length; i++) {
    cur_weight += locations[i]['weight'];

    if (cur_weight > rand_weight) {
      break;
    }
  }

  return locations[i]['url'];
}

function update_mirror_url(download_prefix) {
  var wr_url = weighted_random_url();
  // MUST match versions download_prefix
  var download_prefix = "https://1.na.dl.wireshark.org";
  var da_inner_html = document.getElementById("download-accordion").innerHTML;
  da_inner_html = da_inner_html.replaceAll(download_prefix, wr_url);
  // console.log("=== wlu: " + wr_url);
  // console.log("=== dai: " + da_inner_html);
  document.getElementById("download-accordion").innerHTML = da_inner_html;
}
