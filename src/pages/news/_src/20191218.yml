---
sticky: 0
headline: "Wireshark 3.2.0 Released"
# xmllint --html --xpath "/html/body//div[3]" docbook/release-notes.html | pbcopy
body: |
    <p>Wireshark 3.2.0 has been released.
    Installers for Windows, macOS, and source code are now available.
    </p>
    <div class="paragraph">
    <p>The following features are new (or have been significantly updated)
    since version 3.2.0rc2:</p>
    </div>
    <div class="ulist">
    <ul>
    <li>
    <p>Minor bug fixes.</p>
    </li>
    </ul>
    </div>
    <div class="paragraph">
    <p>The following features are new (or have been significantly updated)
    since version 3.2.0rc1:</p>
    </div>
    <div class="ulist">
    <ul>
    <li>
    <p>Nothing of note.</p>
    </li>
    </ul>
    </div>
    <div class="paragraph">
    <p>The following features are new (or have been significantly updated)
    since version 3.1.1:</p>
    </div>
    <div class="ulist">
    <ul>
    <li>
    <p>Miscellaneous UI fixes and updates.</p>
    </li>
    <li>
    <p>The macOS installer now ships with Qt 5.12.6.
    It previously shipped with Qt 5.12.5.</p>
    </li>
    </ul>
    </div>
    <div class="paragraph">
    <p>The following features are new (or have been significantly updated)
    since version 3.1.0:</p>
    </div>
    <div class="ulist">
    <ul>
    <li>
    <p>Automatic updates are supported on macOS.</p>
    </li>
    <li>
    <p>You can now select multiple packets in the packet list at the same time</p>
    <div class="ulist">
    <ul>
    <li>
    <p>They can be exported as Text by “<span class="keyseq"><kbd>Ctrl</kbd>+<kbd>C</kbd></span>” or “<span class="keyseq"><kbd>Cmd</kbd>+<kbd>C</kbd></span>” and the corresponding
    menu in “<span class="menuseq"><b class="menu">Edit</b> <b class="caret">›</b> <b class="submenu">Copy</b> <b class="caret">›</b> <b class="menuitem">As …​</b></span>”</p>
    </li>
    <li>
    <p>They can be marked/unmarked or ignored/unignored at the same time</p>
    </li>
    <li>
    <p>They can be exported and printed using the corresponding menu entries
    “<span class="menuseq"><b class="menu">File</b> <b class="caret">›</b> <b class="menuitem">Export Specified Packets</b></span>”, “<span class="menuseq"><b class="menu">File</b> <b class="caret">›</b> <b class="menuitem">Export Packet Dissections</b></span>”
    and “<span class="menuseq"><b class="menu">File</b> <b class="caret">›</b> <b class="menuitem">Print</b></span>”</p>
    </li>
    </ul>
    </div>
    </li>
    <li>
    <p>You can now follow HTTP/2 and QUIC streams.</p>
    </li>
    <li>
    <p>You can once again mark and unmark packets using the middle mouse button.
    This feature went missing around 2009 or so.</p>
    </li>
    <li>
    <p>The Windows packages are now built using Microsoft Visual Studio 2019.</p>
    </li>
    <li>
    <p>IOGraph automatically adds a graph for the selected display filter if no
    previous graph exists</p>
    </li>
    <li>
    <p>Action buttons for the display filter bar may be aligned left via the context menu</p>
    </li>
    <li>
    <p>Allow extcaps to be loaded from the personal configuration directory</p>
    </li>
    <li>
    <p>The Windows installers now ship with Qt 5.12.6.
    They previously shipped with Qt 5.12.4.</p>
    </li>
    </ul>
    </div>
    <div class="paragraph">
    <p>The following features are new (or have been significantly updated)
    since version 3.0.0:</p>
    </div>
    <div class="ulist">
    <ul>
    <li>
    <p>You can drag and drop a field to a column header to create a column for that field, or to the
    display filter input to create a display filter.
    If a display filter is applied, the new filter can be added using the same rules as “Apply Filter”</p>
    </li>
    <li>
    <p>You can drag and drop a column entry to the display filter to create a filter for it.</p>
    </li>
    <li>
    <p>You can import profiles from a .zip archive or an existing directory.</p>
    </li>
    <li>
    <p>Dark mode support on macOS and dark theme support on other platforms
    has been improved.</p>
    </li>
    <li>
    <p>Brotli decompression support in HTTP/HTTP2 (requires the brotli library).</p>
    </li>
    <li>
    <p>The build system now checks for a SpeexDSP system library installation. The
    bundled Speex resampler code is still provided as a fallback.</p>
    </li>
    <li>
    <p>WireGuard decryption can now be enabled through keys embedded in a pcapng in
    addition to the existing key log preference (<a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=15571">Bug 15571</a>).</p>
    </li>
    <li>
    <p>A new tap for extracting credentials from the capture file has been added.
    It can be accessed through the <code>-z credentials</code> option in tshark or from the
    “<span class="menuseq"><b class="menu">Tools</b> <b class="caret">›</b> <b class="menuitem">Credentials</b></span>” menu in Wireshark.</p>
    </li>
    <li>
    <p>Editcap can now split files on floating point intervals.</p>
    </li>
    <li>
    <p>Windows .msi packages are now <a href="https://support.microsoft.com/en-us/help/4472027/2019-sha-2-code-signing-support-requirement-for-windows-and-wsus">signed using SHA-2</a>.
    .exe installers are still dual-signed using SHA-1 and SHA-2.</p>
    </li>
    <li>
    <p>The “Enabled Protocols” Dialog now only enables, disables and inverts protocols based on the set filter selection. The protocol type (standard or heuristic) may also be choosen as a filter value.</p>
    </li>
    <li>
    <p>The “<span class="menuseq"><b class="menu">Analyze</b> <b class="caret">›</b> <b class="menuitem">Apply as Filter</b></span>” and “<span class="menuseq"><b class="menu">Analyze</b> <b class="caret">›</b> <b class="menuitem">Prepare a Filter</b></span>” packet list and detail popup menus now show a preview of their respective filters.</p>
    </li>
    <li>
    <p>Protobuf files (*.proto) can now be configured to enable more precise parsing of serialized Protobuf data (such as gRPC).</p>
    </li>
    <li>
    <p>HTTP2 support streaming mode reassembly. To use this feature, subdissectors can register itself to "streaming_content_type" dissector table and return pinfo→desegment_len and pinfo→desegment_offset to tell HTTP2 when to start and how many additional bytes requires when next called.</p>
    </li>
    <li>
    <p>The message of stream gRPC method can now be parsed with supporting of HTTP2 streaming mode reassembly feature.</p>
    </li>
    <li>
    <p>The Windows installers now ship with Qt 5.12.4.
    They previously shipped with Qt 5.12.1.</p>
    </li>
    </ul>
    </div>
    <div class="sect2">
    <h3 id="_new_protocol_support">New Protocol Support</h3>
    <div class="paragraph">
    <p>3GPP BICC MST (BICC-MST), 3GPP log packet (LOG3GPP), 3GPP/GSM Cell Broadcast Service Protocol (cbsp), Bluetooth Mesh Beacon, Bluetooth Mesh PB-ADV, Bluetooth Mesh Provisioning PDU, Bluetooth Mesh Proxy, CableLabs Layer-3 Protocol IEEE EtherType 0xb4e3 (CL3), DCOM IProvideClassInfo, DCOM ITypeInfo, Diagnostic Log and Trace (DLT), Distributed Replicated Block Device (DRBD), Dual Channel Wi-Fi (CL3DCW), EBHSCR Protocol (EBHSCR), EERO Protocol (EERO), evolved Common Public Radio Interface (eCPRI), File Server Remote VSS Protocol (FSRVP), FTDI FT USB Bridging Devices (FTDI FT), Graylog Extended Log Format over UDP (GELF), GSM/3GPP CBSP (Cell Broadcast Service Protocol), Linux net_dm (network drop monitor) protocol, MIDI System Exclusive DigiTech (SYSEX DigiTech), Network Controller Sideband Interface (NCSI), NR Positioning Protocol A (NRPPa) TS 38.455, NVM Express over Fabrics for TCP (nvme-tcp), OsmoTRX Protocol (GSM Transceiver control and data), and Scalable service-Oriented MiddlewarE over IP (SOME/IP)</p>
    </div>
    </div>
