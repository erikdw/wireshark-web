---
sticky: 0
headline: "Wireshark 3.5.0 Development Release"
# xmllint --html --xpath '//div/h2[@id="_whats_new"]/..' docbook/release-notes.html | sed -e 's/^/    /' | pbcopy
body: |
    <div class="sect1">
    <h2 id="_whats_new">What’s New</h2>
    <div class="sectionbody">
    <div class="paragraph">
    <p>Many improvements have been made.
    See the “New and Updated Features” section below for more details.</p>
    </div>
    <div class="sect2">
    <h3 id="_new_and_updated_features">New and Updated Features</h3>
    <div class="paragraph">
    <p>The following features are new (or have been significantly updated) since version 3.4.0:</p>
    </div>
    <div class="ulist">
    <ul>
    <li>
    <p>The Windows installers now ship with Npcap 1.50.</p>
    </li>
    <li>
    <p>A 64-bit Windows PortableApps package is now available.</p>
    </li>
    <li>
    <p>A macOS Arm 64 (Apple Silicon) package is now available.</p>
    </li>
    <li>
    <p>TCP conversations now support a completeness criteria, which facilitates the identification of TCP streams having any
    of opening or closing handshakes, a payload, in any combination. It is accessed with the new tcp.completeness filter.</p>
    </li>
    <li>
    <p>Protobuf fields that are not serialized on the wire (missing in capture files) can now be displayed with default values
    by setting the new 'add_default_value' preference. The default values might be explicitly declared in 'proto2' files,
    or false for bools, first value for enums, zero for numeric types.</p>
    </li>
    <li>
    <p>Wireshark now supports reading Event Tracing for Windows (ETW). A new extcap named ETW reader is created that now can open an etl file,
    convert all events in the file to DLT_ETW packets and write to a specified FIFO destination. Also, a new packet_etw dissector is
    created to dissect DLT_ETW packets so Wireshark can display the DLT_ETW packet header, its message and packet_etw dissector
    calls packet_mbim sub_dissector if its provider matches the MBIM provider GUID.</p>
    </li>
    <li>
    <p>"Follow DCCP stream" feature to filter for and extract the contents of DCCP streams.</p>
    </li>
    <li>
    <p>Wireshark now supports dissecting the rtp packet with OPUS payload.</p>
    </li>
    <li>
    <p>Importing captures from text files is now also possible based on regular expressions. By specifying a regex capturing a single
    packet including capturing groups for relevant fields a textfile can be converted to a libpcap capture file. Supported data
    encodings are plain-hexadecimal, -octal, -binary and base64.
    Also the timestamp format now allows the second-fractions to be placed anywhere in the timestamp and it will be stored with
    nanosecond instead of microsecond precision.</p>
    </li>
    <li>
    <p>Display filter literal strings can now be specified using raw string syntax,
    identical to raw strings in the Python programming language. This is useful
    to avoid the complexity of using two levels of character escapes with regular
    expressions.</p>
    </li>
    <li>
    <p>Significant RTP Player redesign and improvements (see Wireshark User Documentation,
    <a href="https://www.wireshark.org/docs/wsug_html_chunked/ChTelPlayingCalls.html">Playing VoIP Calls</a> and
    <a href="https://www.wireshark.org/docs/wsug_html_chunked/_rtp.html#ChTelRtpPlayer">RTP Player Window</a>)</p>
    <div class="ulist">
    <ul>
    <li>
    <p>RTP Player can play many streams in row</p>
    </li>
    <li>
    <p>UI is more responsive</p>
    </li>
    <li>
    <p>RTP Player maintains playlist, other tools can add/remove streams to it</p>
    </li>
    <li>
    <p>Every stream can be muted or routed to L/R channel for replay</p>
    </li>
    <li>
    <p>Save audio is moved from RTP Analysis to RTP Player. RTP Player saves what was played. RTP Player can save in multichannel .au or .wav.</p>
    </li>
    <li>
    <p>RTP Player added to menu Telephony&gt;RTP&gt;RTP Player</p>
    </li>
    </ul>
    </div>
    </li>
    <li>
    <p>VoIP dialogs (VoIP Calls, RTP Streams, RTP Analysis, RTP Player, SIP Flows) are non-modal, can stay opened on background</p>
    <div class="ulist">
    <ul>
    <li>
    <p>Same tools are provided across all dialogs (Prepare Filter, Analyse, RTP Player …​)</p>
    </li>
    </ul>
    </div>
    </li>
    <li>
    <p>Follow stream is now able to follow SIP calls based on their Call-ID value.</p>
    </li>
    <li>
    <p>Follow stream YAML output format’s has been changed to add timestamps and peers information (for more details see the user’s guide,
    <a href="https://www.wireshark.org/docs/wsug_html_chunked//ChAdvFollowStreamSection.html">Following Protocol Streams</a>)</p>
    </li>
    <li>
    <p>IP fragments between public IPv4 addresses are now reassembled even if they have different VLAN IDs. Reassembly of IP fragments
    where one endpoint is a private (RFC 1918 section 3) or link-local (RFC 3927) IPv4 address continues to take the VLAN ID into
    account, as those addresses can be reused. To revert to the previous behavior and not reassemble fragments with different VLAN IDs,
    turn on the "Enable stricter conversation tracking heuristics" top level protocol preference.</p>
    </li>
    <li>
    <p>USB Link Layer reassembly has been added, which allows hardware captures to be analyzed at the same level as software captures.</p>
    </li>
    <li>
    <p>TShark can now export TLS session keys with the --export-tls-session-keys option.</p>
    </li>
    <li>
    <p>Wireshark participated in the Google Season of Docs 2020 and the User’s Guide has been extensively updated.</p>
    </li>
    <li>
    <p>Format of export to CSV in RTP Stream Analysis dialog was slightly changed. First line of export contains names of columns as in other CSV exports.</p>
    </li>
    <li>
    <p>Wireshark now supports the Turkish language.</p>
    </li>
    </ul>
    </div>
    </div>
    <div class="sect2">
    <h3 id="_new_file_format_decoding_support">New File Format Decoding Support</h3>
    <div class="paragraph">
    <p>Vector Informatik Binary Log File (BLF)</p>
    </div>
    </div>
    <div class="sect2">
    <h3 id="_new_protocol_support">New Protocol Support</h3>
    <div class="paragraph">
    <p>Bluetooth Link Manager Protocol (BT LMP), E2 Application Protocol (E2AP), Event Tracing for Windows (ETW), High-Performance Connectivity Tracer (HiPerConTracer), Kerberos SPAKE, Linux psample protocol, Local Interconnect Network (LIN), Microsoft Task Scheduler Service, O-RAN E2AP, O-RAN fronthaul UC-plane (O-RAN), Opus Interactive Audio Codec (OPUS), PDU Transport Protocol, R09.x (R09), RDP Dynamic Channel Protocol (DRDYNVC), Real-Time Publish-Subscribe Virtual Transport (RTPS-VT), Real-Time Publish-Subscribe Wire Protocol (processed) (RTPS-PROC), Shared Memory Communications (SMC), Signal PDU, SparkplugB, State Synchronization Protocol (SSyncP), Tagged Image File Format (TIFF), TP-Link Smart Home Protocol, and World of Warcraft World (WOWW)</p>
    </div>
    </div>
    <div class="sect2">
    <h3 id="_updated_protocol_support">Updated Protocol Support</h3>
    <div class="paragraph">
    <p>Too many protocols have been updated to list here.</p>
    </div>
    </div>
    <div class="sect2">
    <h3 id="_new_and_updated_capture_file_support">New and Updated Capture File Support</h3>
    <div class="paragraph">
    <p>Vector Informatik Binary Log File (BLF)</p>
    </div>
    </div>
    </div>
    </div>
