---
sticky: 0
headline: "Wireshark 4.2.0rc1 Release Candidate"
# xmllint --html --xpath '//div/h2[@id="_whats_new"]/..' doc/release-notes.html | sed -e 's/^/    /' -e 's/\\/\&bsol;/g' | pbcopy
body: |
    <div class="sect1">
    <h2 id="_whats_new">What’s New</h2>
    <div class="sectionbody">
    <div class="paragraph">
    <p>Wireshark supports dark mode on Windows.</p>
    </div>
    <div class="paragraph">
    <p>A Windows installer for Arm64 has been added.</p>
    </div>
    <div class="paragraph">
    <p>Packet list sorting has been improved.</p>
    </div>
    <div class="paragraph">
    <p>Wireshark and TShark are now better about generating valid UTF-8 output.</p>
    </div>
    <div class="paragraph">
    <p>A new display filter feature for filtering raw bytes has been added.</p>
    </div>
    <div class="paragraph">
    <p>Display filter autocomplete is smarter about not suggesting invalid syntax.</p>
    </div>
    <div class="paragraph">
    <p><span class="menuseq"><b class="menu">Tools</b> <b class="caret">›</b> <b class="menuitem">MAC Address Blocks</b></span> can lookup a MAC address in the IEEE OUI registry.</p>
    </div>
    <div class="paragraph">
    <p>The <em>enterprises</em>, <em>manuf</em>, and <em>services</em> configuration files have been compiled in for improved start-up times.</p>
    </div>
    <div class="paragraph">
    <p>The installation target no longer installs development headers by default.</p>
    </div>
    <div class="paragraph">
    <p>The Wireshark installation is relocatable on Linux (and other ELF platforms
    with support for relative RPATHs).</p>
    </div>
    <div class="paragraph">
    <p>Wireshark can be compiled on Windows using <a href="https://www.msys2.org/">MSYS2</a>.
    Check the Developer’s guide for instructions.</p>
    </div>
    <div class="paragraph">
    <p>Wireshark can be cross-compiled for Windows using Linux.
    Check the Developer’s guide for instructions.</p>
    </div>
    <div class="paragraph">
    <p><span class="menuseq"><b class="menu">Tools</b> <b class="caret">›</b> <b class="menuitem">Browser (SSL Keylog)</b></span> can launch your web browser with the
    SSLKEYLOGFILE environment variable set to the appropriate value.</p>
    </div>
    <div class="paragraph">
    <p>Windows installer file names now have the format Wireshark-&lt;version&gt;-&lt;architecture&gt;.exe.</p>
    </div>
    <div class="paragraph">
    <p>Many other improvements have been made.
    See the “New and Updated Features” section below for more details.</p>
    </div>
    <div class="sect2">
    <h3 id="_bug_fixes">Bug Fixes</h3>
    <div class="paragraph">
    <p>The following bugs have been fixed:</p>
    </div>
    <div class="ulist">
    <ul>
    <li>
    <p><a href="https://gitlab.com/wireshark/wireshark/-/issues/18413">Issue 18413</a> - RTP player do not play audio frequently on Windows builds with Qt6.</p>
    </li>
    <li>
    <p><a href="https://gitlab.com/wireshark/wireshark/-/issues/18510">Issue 18510</a> - Playback marker does not move after resume with Qt6.</p>
    </li>
    </ul>
    </div>
    </div>
    <div class="sect2">
    <h3 id="_new_and_updated_features">New and Updated Features</h3>
    <div class="paragraph">
    <p>The following features are new (or have been significantly updated) since version 4.1.0:</p>
    </div>
    <div class="ulist">
    <ul>
    <li>
    <p>Improved dark mode support.</p>
    </li>
    </ul>
    </div>
    <div class="paragraph">
    <p>The following features are new (or have been significantly updated) since version 4.0.0:</p>
    </div>
    <div class="ulist">
    <ul>
    <li>
    <p>The Windows installers now ship with Qt 6.5.3.
    They previously shipped with Qt 6.2.3.</p>
    </li>
    </ul>
    </div>
    <div class="ulist">
    <ul>
    <li>
    <p>The API has been updated to ensure that the dissection engine produces valid UTF-8 strings.</p>
    </li>
    <li>
    <p>Wireshark now builds with Qt6 by default. To use Qt5 instead pass USE_qt6=OFF to CMake.</p>
    </li>
    <li>
    <p>The "ciscodump" extcap supports Cisco IOS XE 17.x.</p>
    </li>
    <li>
    <p>The default interval between GUI updates when capturing has been decreased
    from 500ms to 100ms, and is now configurable.</p>
    </li>
    <li>
    <p>The <strong>-n</strong> option also now disables IP address geolocation information lookup
    in configured MaxMind databases (and geolocation lookup can be enabled with
    <strong>-Ng</strong>.) This is most relevant for TShark, where geolocation lookups are
    synchronous.</p>
    </li>
    <li>
    <p>The display filter drop-down list is now sorted by "most recently used" instead
    of "most recently created".</p>
    </li>
    <li>
    <p>Display filter syntax-related changes:</p>
    <div class="ulist">
    <ul>
    <li>
    <p>It is now possible to filter on raw packet data for any field by using the syntax <code>@some.field == &lt;bytes…​&gt;</code>.
    This can be useful to filter on malformed UTF-8 strings, among other use cases where it is necessary to
    look at the field’s raw data.</p>
    </li>
    <li>
    <p>Negation (unary minus) now works with any display filter arithmetic expression.</p>
    </li>
    <li>
    <p>Using the slice operator with strings produces a string. Previously it
    would produce a byte array. This is useful to index/slice UTF-8 multibyte strings.
    String byte slices can still be obtained using the "@" (raw operator) prefix.</p>
    </li>
    <li>
    <p>Arithmetic expressions are allowed as set elements.</p>
    </li>
    <li>
    <p>Absolute date and time values can be written as Unix time.</p>
    </li>
    <li>
    <p>The limitation where a minus sign needed to be preceded by a space character
    has been removed.</p>
    </li>
    <li>
    <p>Added XOR logical operator.</p>
    </li>
    <li>
    <p>Fixed the implementation of <code>all …​ in</code> membership operator
    (<a href="https://gitlab.com/wireshark/wireshark/-/issues/19188">#19188</a>).</p>
    </li>
    <li>
    <p>When parsing absolute time values the display filter engine has learned
    to understand timezones as specified in
    <a href="https://man.netbsd.org/strptime.3">strptime(3)</a>, including some common
    North American designations. Arbitrary timezone names are not supported
    however. Previously only ISO8601 offsets and the "UTC" designation was understood.</p>
    </li>
    <li>
    <p>Writing value strings without double quotes is deprecated and will generate
    a warning. Value strings are integer or boolean values that can be represented
    using a user-friendly textual format, such as "Set"/"Unset" instead of numerical
    values like 1 and 0. It is now a requirement that value strings need to be
    written enclosed in double-quotes.</p>
    </li>
    <li>
    <p>The deprecated ~≃ operator symbol has been removed. It was replaced by !== in version 4.0.</p>
    </li>
    </ul>
    </div>
    </li>
    <li>
    <p>Running the test suite requires the <a href="https://pypi.org/project/pytest/">pytest</a>
    Python module. The emulation layer that allowed running tests without pytest
    installed has been removed.</p>
    </li>
    <li>
    <p>When saving files or exporting packets after changing their time with the
    "Time Shift" dialog, the shifted time is written to the new file.</p>
    </li>
    <li>
    <p>TLS secrets used in decrypting packets can be embedded (or discarded) from
    the capture file via the GUI, similar to the options --inject-secrets and
    --discard-all-secrets in editcap.</p>
    </li>
    <li>
    <p>The text of any configured column (displayed or hidden) can be filtered
    anywhere that filters are used - in display filters, filters in taps, coloring
    rules, Wireshark read filters, and the -Y, -R, and -e options to TShark,
    the "Apply as Filter" GUI option, etc.</p>
    <div class="ulist">
    <ul>
    <li>
    <p>The filter field names are prefixed by "_ws.col", followed by a lowercase
    version of the COL_ name found in epan/column-utils.h, e.g. "_ws.col.info"
    or "_ws.col.protocol"</p>
    </li>
    <li>
    <p>Using the column names as a filter is slower than other filter types
    because the columns must be constructed, so when the same filtering
    can be achieved via other fields, prefer that.</p>
    </li>
    </ul>
    </div>
    </li>
    <li>
    <p>The external name resolution text files "manuf", "enterprises" and "services"
    have been removed and replaced with static binary data. You can dump the
    respective internal data using <code>tshark -G manuf|enterprises|services</code>.</p>
    </li>
    <li>
    <p>The "manuf" file is now also read from the personal configuration folder,
    and is profile-based.</p>
    </li>
    <li>
    <p>The Lua console dialogs under the <b class="menuref">Tools</b> menu were refactored and redesigned.
    It now consists of a single dialog window for input and output.</p>
    </li>
    <li>
    <p>Wireshark now shows byte units in the statistics in the user-selected language
    (uses the system default language by default).</p>
    </li>
    <li>
    <p>Packet list sorting has been improved:</p>
    <div class="ulist">
    <ul>
    <li>
    <p>When sorting packet list with a filter applied, only the visible packets are
    sorted, which greatly increases sorting speed.</p>
    </li>
    <li>
    <p>The cache size for column text is limited to a default of 10000 rows,
    which limits the maximum memory usage. The maximum value can be changed in
    Preferences→Appearance→Layout</p>
    </li>
    <li>
    <p>Due to the above, columns that require packet dissection can only be sorted
    if the number of visible rows is less than the cache size. If there are
    more rows visible, a warning will appear. Columns that do not require packet
    dissection (those that calculated directly from the capture file frame
    headers, such as packet number, time, and frame length) can be sorted with
    any number of visible rows.</p>
    </li>
    <li>
    <p>Sorting can be interrupted.</p>
    </li>
    </ul>
    </div>
    </li>
    <li>
    <p>When changing the dissector via the "Decode As" table for values that
    have default dissectors registered, selecting "(none)" will select
    no dissection (while still allowing heuristic dissectors to attempt to
    dissect.) The previous behavior was to reset the dissector to the default.
    To facilitate resetting the dissector, the default dissector is now sorted
    at the top of the list of possible dissector options.</p>
    </li>
    <li>
    <p>The personal extcap plugin folder location on Unix has been changed to
    follow existing conventions for architecture-dependent files.
    The extcap personal folder is now <code>$HOME/.local/lib/wireshark/extcap</code>.
    Previously it was <code>$XDG_CONFIG_HOME/wireshark/extcap</code>.</p>
    </li>
    <li>
    <p>The "init.lua" file is now loaded from any of the Lua plugin directories.
    Previously it was loaded from the personal configuration directory. (For
    backward-compatibility this is still allowed; note that deprecated features
    may be removed in a future release).</p>
    </li>
    <li>
    <p>Installation of development headers must be done explicitly using the CMake
    command <code>cmake --install &lt;builddir&gt; --component Development</code>.</p>
    </li>
    <li>
    <p>The Windows build has a new SpeexDSP external dependency (<a href="https://www.speex.org" class="bare">https://www.speex.org</a>).
    The speex code that was previously bundled has been removed.</p>
    </li>
    <li>
    <p>New <code>--print-timers</code> option added to TShark.</p>
    </li>
    </ul>
    </div>
    </div>
    <div class="sect2">
    <h3 id="_removed_features_and_support">Removed Features and Support</h3>
    <div class="ulist">
    <ul>
    <li>
    <p>With the addition of the universal and consistent filtering support for
    column text, the previous support in the -e option to TShark for displaying
    column text via the column title has been removed in general. Those field names
    cannot be used elsewhere (as they may not be legal filter names) and create
    confusion if more than one column has the same title or if a column is renamed.
    Prefer the column format instead, e.g. "_ws.col.info" for "_ws.col.Info".
    However, for backwards compatibility with existing tools and scripts, the
    titles of the default columns can continue to be used with <code>tshark -e</code>
    (but not elsewhere.)</p>
    </li>
    <li>
    <p>The bundled script "dtd_gen.lua" that was disabled by default has been removed
    from the installation. <a href="https://wiki.wireshark.org/Contrib">It can be found in the Wireshark Wiki under "Contrib"</a>.</p>
    </li>
    <li>
    <p>The Wi-Fi NAN dissector filter name has been changed from 'nan' to 'wifi_nan'.</p>
    </li>
    </ul>
    </div>
    </div>
    <div class="sect2">
    <h3 id="_new_file_format_decoding_support">New File Format Decoding Support</h3>
    <div class="paragraph">
    <p>RTPDump</p>
    </div>
    </div>
    <div class="sect2">
    <h3 id="_new_protocol_support">New Protocol Support</h3>
    <div class="paragraph">
    <p>Aruba UBT, ASAM Capture Module Protocol (CMP), ATSC Link-Layer Protocol (ALP), DECT DLC protocol layer (DECT-DLC), DECT NWK protocol layer (DECT-NWK), DECT proprietary Mitel OMM/RFP Protocol (also named AaMiDe), Digital Object Identifier Resolution Protocol (DO-IRP), Discard Protocol, FiRa UWB Controller Interface (UCI), FiveCo’s Register Access Protocol (5CoRAP), Fortinet FortiGate Cluster Protocol (FGCP), GPS L1 C/A LNAV navigation messages, GSM Radio Link Protocol (RLP), H.224, High Speed Fahrzeugzugang (HSFZ), Hypertext Transfer Protocol version 3 (HTTP/3), ID3v2, IEEE 802.1CB (R-TAG), Iperf3, JSON 3GPP, Low Level Signalling (ATSC3 LLS), Management Component Transport Protocol (MCTP), Management Component Transport Protocol - Control Protocol (MCTP CP), Matter home automation protocol, Microsoft Delivery Optimization, Multi-Drop Bus (MDB), Non-volatile Memory Express - Management Interface (NVMe-MI) over MCTP, RDP audio output virtual channel Protocol (rdpsnd), RDP clipboard redirection channel Protocol (cliprdr), RDP Program virtual channel Protocol (RAIL), SAP Enqueue Server (SAPEnqueue), SAP GUI (SAPDiag), SAP HANA SQL Command Network Protocol (SAPHDB), SAP Internet Graphic Server (SAP IGS), SAP Message Server (SAPMS), SAP Network Interface (SAPNI), SAP Router (SAPROUTER), SAP Secure Network Connection (SNC), SBAS L1 Navigation Messages (SBAS L1), SINEC AP1 Protocol (SINEC AP), SMPTE ST2110-20 (Uncompressed Active Video), Train Real-Time Data Protocol (TRDP), UBX protocol of u-blox GNSS receivers (UBX), UDP Tracker Protocol for BitTorrent (BT-Tracker), UWB UCI Protocol, Video Protocol 9 (VP9), VMware HeartBeat, Windows Delivery Optimization (MS-DO), Z21 LAN Protocol (Z21), Zabbix, ZigBee Direct (ZBD), and Zigbee TLV</p>
    </div>
    </div>
    <div class="sect2">
    <h3 id="_updated_protocol_support">Updated Protocol Support</h3>
    <div class="ulist">
    <ul>
    <li>
    <p>JSON: The dissector now has a preference to enable/disable "unescaping"
    of string values. By default it is off. Previously it was always on.</p>
    </li>
    <li>
    <p>JSON: The dissector now supports "Display JSON in raw form".</p>
    </li>
    <li>
    <p>IPv6: The dissector has a new preference to show some semantic details
    about addresses (default off).</p>
    </li>
    <li>
    <p>IPv6: The dissector now supports dissecting the
    <a href="https://www.ipv6plus.net/Phase3/apn6/">
    Application-aware IPv6 Networking (APN6) option</a>
    in the Hop-by-Hop Options Header (HBH) and Destination Options Header (DOH),
    including all three types of APN ID,
    which are 32-bit, 64-bit and 128-bit in length.</p>
    </li>
    <li>
    <p>XML: The dissector now supports display character according to the "encoding"
    attribute of the XML declaration, and has a new preference to set default
    character encoding for some XML document without "encoding" attribute.</p>
    </li>
    <li>
    <p>SIP: The dissector now has a new preference to set default charset for
    displaying the body of SIP messages in raw text view.</p>
    </li>
    <li>
    <p>HTTP: The dissector now supports dissecting chunked data in streaming reassembly
    mode. Subdissectors of HTTP can register itself in "streaming_content_type"
    subdissector table for enabling streaming reassembly mode while transferring in
    chunked encoding. This feature ensures the server stream messages of GRPC-Web
    over HTTP/1.1 can be dissected even if the last chunk is absent.</p>
    </li>
    <li>
    <p>The media type dissector table now properly treats media types and subtypes
    as case-insensitive automatically, per RFC 6838. Media types no longer need
    to be lower cased before registering or looking up in the table.</p>
    </li>
    <li>
    <p>CFM: The dissector has been overhauled and updated to the level of IEEE std
    802.1Q-2022 and ITU-T Rec. G.8013/Y.1371 (08/2015). This includes dissection
    of additional PDU types and TLVs as well as deeper dissection of existing PDUs
    and TLVs.</p>
    </li>
    </ul>
    </div>
    <div class="paragraph">
    <p>Too many other protocol updates have been made to list them all here.</p>
    </div>
    </div>
    <div class="sect2">
    <h3 id="_new_and_updated_codec_support">New and Updated Codec support</h3>
    <div class="paragraph">
    <p>Adaptive Multi-Rate (AMR), if compiled with <a href="https://sourceforge.net/projects/opencore-amr/">opencore-amr</a>.</p>
    </div>
    </div>
    <div class="sect2">
    <h3 id="_major_api_changes">Major API Changes</h3>
    <div class="ulist">
    <ul>
    <li>
    <p>Lua function "package.prepend_path" has been removed. If you need it please
    consider adding your own package.path customization code or installing your
    dependencies in Wireshark’s default paths.</p>
    </li>
    <li>
    <p>The reassemble_streaming_data_and_call_subdissector() API has been added to provide a simpler way to
    reassemble the streaming data of a high level protocol that is not on top of TCP.</p>
    </li>
    <li>
    <p>Some of the API now uses C99 types instead of GLib types. <a href="https://gitlab.com/wireshark/wireshark/-/issues/19116">Issue 19116</a></p>
    </li>
    </ul>
    </div>
    </div>
    </div>
    </div>