---
sticky: 0
headline: "Wireshark 1.99.3 Development Release"
body: |
    <p>
    Wireshark 1.99.3 has been released.
    This is an experimental release intended to test features that will go into
    Wireshark 2.0.
    Installers for Windows, OS X, and source code
    are now available.
    </p>    
    <p>The following features are new (or have been significantly updated)
    since version 1.99.2:</p><div class="itemizedlist"><ul class="itemizedlist" style="list-style-type: disc; "><li class="listitem"><p class="simpara">
    Qt port:
    </p><div class="itemizedlist"><ul class="itemizedlist" style="list-style-type: circle; "><li class="listitem">
    Several bugs have been fixed.
    </li><li class="listitem">
    You can now open a packet in a new window.
    </li><li class="listitem">
    The Bluetooth ATT Server Attributes dialog has been added.
    </li><li class="listitem">
    The Coloring Rules dialog has been added.
    </li><li class="listitem">
    Many translations have been updated. Chinese, Italian and Polish
       translations are complete.
    </li><li class="listitem">
    General user interface and usability improvements.
    </li><li class="listitem">
    Automatic scrolling during capture now works.
    </li><li class="listitem">
    The related packet indicator has been updated.
    </li></ul></div></li></ul></div>
    <p>The following features are new (or have been significantly updated)
    since version 1.99.1:</p><div class="itemizedlist"><ul class="itemizedlist" type="disc"><li class="listitem"><p class="simpara">
    Qt port:
    </p><div class="itemizedlist"><ul class="itemizedlist" style="list-style-type: circle; "><li class="listitem">
    The welcome screen layout has been updated.
    </li><li class="listitem">
    The Preferences dialog no longer crashes on Windows.
    </li><li class="listitem">
    The packet list header menu has been added.
    </li><li class="listitem">
    Statistics tree plugins are now supported.
    </li><li class="listitem">
    The window icon is now displayed properly in the Windows taskbar.
    </li><li class="listitem">
    A packet list an byte view selection bug has been fixed (<a class="ulink" href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=10896" target="_top">Bug 10896</a>)
    </li><li class="listitem">
    The RTP Streams dialog has been added.
    </li><li class="listitem">
    The Protocol Hierarchy Statistics dialog has been added.
    </li></ul></div></li></ul></div><p>The following features are new (or have been significantly updated)
    since version 1.99.0:</p><div class="itemizedlist"><ul class="itemizedlist" style="list-style-type: disc; "><li class="listitem"><p class="simpara">
    Qt port:
    </p><div class="itemizedlist"><ul class="itemizedlist" type="circle"><li class="listitem">
    You can now show and hide toolbars and major widgets using the View menu.
    </li><li class="listitem">
    You can now set the time display format and precision.
    </li><li class="listitem">
    The byte view widget is much faster, particularly when selecting large
    reassembled packets.
    </li><li class="listitem">
    The byte view is explorable. Hovering over it highlights the corresponding
    field and shows a description in the status bar.
    </li><li class="listitem">
    An Italian translation has been added.
    </li><li class="listitem">
    The Summary dialog has been updated and renamed to Capture File Properties.
    </li><li class="listitem">
    The VoIP Calls and SIP Flows dialogs have been added.
    </li></ul></div></li></ul></div><p>The following features are new (or have been significantly updated)
    since version 1.12.0:</p><div class="itemizedlist"><ul class="itemizedlist" type="disc"><li class="listitem">
    The I/O Graph in the Gtk+ UI now supports an unlimited number of data points
    (up from 100k).
    </li><li class="listitem">
    TShark now resets its state when changing files in ring-buffer mode.
    </li><li class="listitem">
    Expert Info severities can now be configured.
    </li><li class="listitem">
    Wireshark now supports external capture interfaces.  External capture
    interfaces can be anything from a tcpdump-over-ssh pipe to a program that
    captures from proprietary or non-standard hardware.  This functionality is not
    available in the Qt UI yet.
    </li><li class="listitem"><p class="simpara">
    Qt port:
    </p><div class="itemizedlist"><ul class="itemizedlist" type="circle"><li class="listitem">
    The Qt UI is now the default (program name is wireshark).
    </li><li class="listitem">
    A Polish translation has been added.
    </li><li class="listitem">
    The Interfaces dialog has been added.
    </li><li class="listitem">
    The interface list is now updated when interfaces appear or disappear.
    </li><li class="listitem">
    The Conversations and Endpoints dialogs have been added.
    </li><li class="listitem">
    A Japanese translation has been added.
    </li><li class="listitem">
    It is now possible to manage remote capture interfaces.
    </li><li class="listitem">
    Windows: taskbar progress support has been added.
    </li><li class="listitem">
    Most toolbar actions are in place and work.
    </li><li class="listitem">
    More command line options are now supported
    </li></ul></div></li></ul></div>
              <p>
        </p>
    <p>
    Official releases are available right now from the
    <a href="../download.html">download page</a>.
    </p>
