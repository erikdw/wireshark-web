---
sticky: 0
headline: "Wireshark 2.4.0 Released"
body: |
    <p>
    Wireshark 2.4.0 has been released.
    Installers for Windows, macOS, and source code are now available.
    </p>
    <h3>New or significantly updated features since version 2.2.0</h3>
    <div class="itemizedlist"><ul class="itemizedlist" style="list-style-type: disc; "><li class="listitem">
    Experimental 32-bit and 64-bit Windows Installer (.msi) packages are available.
    It is recommended that you use these independently of the NSIS (.exe) installers.
    That is, you should make sure the NSIS package is completely uninstalled before
    installing the Windows Installer package and vice-versa.
    </li><li class="listitem">
    Source packages are now compressed using xz instead of bzip2.
    </li><li class="listitem">
    The legacy (GTK+) UI is disabled by default in the Windows installers.
    </li><li class="listitem">
    The legacy (GTK+) UI is disabled by default in the development environment (Autotools and CMake).
    </li><li class="listitem">
    SS7 Point Codes can now be resolved into names with a hosts-like file.
    </li><li class="listitem">
    Wireshark can now go fullscreen to have more room for packets.
    </li><li class="listitem">
    TShark can now export objects like the other GUI interfaces.
    </li><li class="listitem">
    Support for G.722 and G.726 codecs in the RTP Player (via the SpanDSP library).
    </li><li class="listitem">
    You can now choose the output device when playing RTP streams.
    </li><li class="listitem">
    Added support for dissectors to include a unit name natively in their hf field.
    A field can now automatically append "seconds" or "ms" to its value without
    additional printf-style APIs.
    </li><li class="listitem">
    The Default profile can now be reset to default values.
    </li><li class="listitem">
    You can move back and forth in the selection history in the Qt UI.
    </li><li class="listitem">
    IEEE 802.15.4 dissector now uses an UAT for decryption keys. The original
    decryption key preference has been obsoleted.
    </li><li class="listitem">
    Extcap utilities can now provide configuration for a GUI interface toolbar to
    control the extcap utility while capturing.
    </li><li class="listitem">
    Extcap utilities can now validate the capture filter.
    </li><li class="listitem">
    Display filter function len() can now be used on all string and byte fields.
    </li><li class="listitem">
    Added an experimental timeline view for 802.11 wireless packet data which can
    be enabled via the "802.11 radio information" preferences.
    </li><li class="listitem">
    Added TLS 1.3 (draft 21) dissection and decryption support (<a class="ulink" href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=12779" target="_top">Bug 12779</a>).
    </li><li class="listitem">
    The (D)TLS Application Layer protocol (e.g. HTTP or CoAP) can now be changed
    via the Decode As dialog.
    </li><li class="listitem">
    The RSA keys dialog for SSL keys has improved feedback for invalid settings
    and no longer requires the IP address, Port or Protocol fields to be set in
    addition to the Key File.
    </li><li class="listitem">
    TCP Analysis will detect and flag more spurious retransmissions.
    </li></ul></div>
    <h3>New Protocol Support</h3>
    <p>
    Bluetooth HCI Vendor Intel,
    CAN FD,
    Citrix NetScaler Metric Exchange Protocol,
    Citrix NetScaler RPC Protocol,
    DirectPlay 8 protocol,
    Ericsson A-bis P-GSL,
    Ericsson A-bis TFP (Traffic Forwarding Protocol),
    Facebook Zero,
    Fc00/cjdns Protocol,
    Generic Netlink (genl),
    GSM Osmux,
    GSMTAP based logging,
    Health Level 7 (HL7),
    High-speed SECS message service (HSMS),
    HomePNA,
    IndigoCare iCall protocol,
    IndigoCare Netrix protocol,
    iPerf2,
    ISO 15765,
    Linux 802.11 Netlink (nl80211),
    Local Service Discovery (LSD),
    M2 Application Protocol,
    Mesh Link Establishment (MLE),
    MUDURL,
    Netgear Ensemble Protocol,
    NetScaler HA Protocol,
    NetScaler Metric Exchange Protocol,
    NetScaler RPC Protocol,
    NM protocol,
    Nordic BLE Sniffer,
    NVMe,
    NVMe Fabrics RDMA,
    OBD-II PIDs,
    OpenThread simulator,
    RFTap Protocol,
    SCTE-35 Digital Program Insertion Messages,
    Snort Post-dissector,
    Thread CoAP,
    UDP based FTP w/ multicast (UFTP and UFTP4),
    Unified Diagnostic Services (UDS),
    vSocket,
    Windows Cluster Management API (clusapi),
    and X-Rite i1 Display Pro (and derivatives) USB protocol
    </p>
    <p>
    Official releases are available right now from the
    <a href="../download.html">download page</a>.
    </p>
