file_date: July 20, 2009
start_version: 0.9.2
end_version: 1.2.0
fixed_versions: 1.2.1

name: |
    Multiple vulnerabilities in Wireshark

affected_versions: |
    {{ start_version }} up to and including
    {{ end_version }}

body: |
    <p>
    Wireshark {{ fixed_versions }} fixes the following vulnerabilities:
    </p>
    
    <ul>
    
    <li>
      The IPMI dissector could overrun a buffer.
      <!-- Fixed in trunk: r28801 -->
      <!-- Fixed in trunk-1.2: r29099 -->
      (<ulink url="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=3559">Bug
      3559</ulink>)
      <br>
      Versions affected: 1.2.0
      <br>
      <ulink url="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-2559">CVE-2009-2559</ulink>
    
    <li>
      The AFS dissector could crash.
      <!-- Fixed in trunk: r28815 -->
      <!-- Fixed in trunk-1.0: r29702 -->
      <!-- Fixed in trunk-1.2: r29099 -->
      (<ulink url="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=3564">Bug
      3564</ulink>)
      <br>
      Versions affected: 0.9.2 to 1.2.0
      <br>
      <ulink url="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-2562">CVE-2009-2562</ulink>
    
    <li>
      The Infiniband dissector could crash on some platforms.
      <!-- Fixed in trunk: r28839 -->
      <!-- Fixed in trunk-1.0: r29702 -->
      <!-- Fixed in trunk-1.2: r29099 -->
      <br>
      Versions affected: 1.0.6 to 1.2.0
      <br>
      <ulink url="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-2563">CVE-2009-2563</ulink>
    
    <li>
      The Bluetooth L2CAP dissector could crash.
      <!-- Fixed in trunk: r28884 -->
      <!-- Fixed in trunk-1.2: r29101 -->
      (<ulink url="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=3572">Bug
      3572</ulink>)
      <br>
      Versions affected: 1.2.0
      <br>
      <ulink url="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-2560">CVE-2009-2560</ulink>
    
    <li>
      The RADIUS dissector could crash.
      <!-- Fixed in trunk: r28891 -->
      <!-- Fixed in trunk-1.2: r29101 -->
      (<ulink url="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=3578">Bug
      3578</ulink>)
      <br>
      Versions affected: 0.10.13 to 1.0.9, 1.2.0
      <br>
      <ulink url="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-2560">CVE-2009-2560</ulink>
    
    <li>
      The MIOP dissector could crash.
      <!-- Fixed in trunk: r28963 -->
      <!-- Fixed in trunk-1.2: rr29101 -->
      (<ulink url="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=3652">Bug
      3652</ulink>)
      <br>
      Versions affected: 1.2.0
      <br>
      <ulink url="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-2560">CVE-2009-2560</ulink>
    
    <li>
      The sFlow dissector could use excessive CPU and memory.
      <!-- Fixed in trunk: r28897 -->
      <!-- Fixed in trunk-1.2: rr29101 -->
      (<ulink url="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=3570">Bug
      3570</ulink>)
      <br>
      Versions affected: 1.2.0
      <br>
      <ulink url="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-2561">CVE-2009-2561</ulink>
    
    </ul>
    
    <h2>Impact</h2>
    
    <p>
    It may be possible to make Wireshark crash remotely or by convincing
    someone to read a malformed packet trace file.
    </p>
    
    <h2>Resolution</h2>
    
    <p>
    Upgrade to Wireshark {{ fixed_versions }} or later.
    </p>
    
    <p>
    If are running Wireshark {{ end_version }} or earlier (including Ethereal)
    and cannot upgrade, you can work around each of the problems listed above by
    doing the following:
    </p>
    
    <ul class="item-list">
    
      <li>Disable the affected dissectors:
    
        <ul>
    
          <li>Select <em>Analyze&#8594;Enabled Protocols...</em> from the menu.
    
          <li>Make sure "AFS (RX)", "Infiniband", "IPMI/ATCA", "L2CAP", "MIOP",
          "RADIUS", and "sFlow" are all un-checked.
    
          <li>Click "Save", then click "OK".
    
        </ul>
    
    </ul>
    
    
    
