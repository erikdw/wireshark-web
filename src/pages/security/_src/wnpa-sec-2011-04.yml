file_date: March 1, 2011
start_version: 1.4.0
end_version: 1.4.3
fixed_versions: 1.4.4

name: |
    MAC-LTE, ENTTEC, and ASN.1 BER vulnerabilities in Wireshark

affected_versions: |
    {{ start_version }} <!-- up to and including
    {{ end_version }} -->

related: |
    <a href="wnpa-sec-2011-03.html">wnpa-sec-2011-03</a>
    (MAC-LTE and ENTTEC vulnerabilities in Wireshark version 1.2.0 to 1.2.14 )

body: |
    <p>
    Wireshark {{ fixed_versions }} fixes the following vulnerabilities:
    </p>
    
    <p>
    <ul>
    
    <li>
        Huzaifa Sidhpurwala of the Red Hat Security Response Team discovered
        that Wireshark could free an uninitialized pointer while reading a
        malformed pcap-ng file.
        <!-- Fixed in trunk: r35791 -->
        <!-- Fixed in trunk-1.4: r35792 -->
        <!-- Fixed in trunk-1.2: r35793 -->
        (<a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=5652">Bug
        5652</a>)
      </br>
      Versions affected: 1.2.0 to 1.2.14 and 1.4.0 to 1.4.3.</br>
        <a href="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2011-0538">CVE-2011-0538</a>
      </br>
    
    
    <li>
        Huzaifa Sidhpurwala of the Red Hat Security Response Team discovered
        that a large packet length in a pcap-ng file could crash Wireshark.
        <!-- Fixed in trunk: r35855 -->
        <!-- Fixed in trunk-1.4: r35857 -->
        <!-- Fixed in trunk-1.2: r35858 -->
        (<a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=5661">Bug
        5661</a>)
      </br>
      Versions affected: 1.2.0 to 1.2.14 and 1.4.0 to 1.4.3.</br>
      <!--
        <a href="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2011-????">CVE-2011-????</a>
      </br>
      -->
    
    
    <li>
        Wireshark could overflow a buffer while reading a Nokia DCT3
        trace file.
        <!-- Fixed in trunk: r35953 -->
        <!-- Fixed in trunk-1.4: r36050 -->
        <!-- Fixed in trunk-1.2: r36051 -->
      </br>
      Versions affected: 1.2.0 to 1.2.14 and 1.4.0 to 1.4.3.</br>
        <a href="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2011-0713">CVE-2011-0713</a>
      </br>
    
    
    <li>
        Paul Makowski working for SEI/CERT discovered that Wireshark on 32
        bit systems could crash while reading a malformed 6LoWPAN packet.
        <!-- Fixed in trunk: r36037 -->
        <!-- Fixed in trunk-1.4: r36036 -->
        (<a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=5722">Bug
        5722</a>)
      </br>
      Versions affected: 1.4.0 to 1.4.3.</br>
      <!--
        <a href="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2011-????">CVE-2011-????</a>
      </br>
      -->
    
    
    <li>
        joernchen of Phenoelit discovered that the LDAP and SMB dissectors
        could overflow the stack.
        <!-- Fixed in trunk: r36029 -->
        <!-- Fixed in trunk-1.4: r36053 -->
        <!-- Fixed in trunk-1.2: r36054 -->
        (<a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=5717">Bug
        5717</a>)
      </br>
      Versions affected: 1.2.0 to 1.2.14 and 1.4.0 to 1.4.3. (Prior versions including 1.0.x are also affected.)</br>
      <!--
        <a href="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2011-????">CVE-2011-????</a>
      </br>
      -->
    
    
    <li>
        Xiaopeng Zhang of Fortinet's Fortiguard Labs discovered that large
        LDAP Filter strings can consume excessive amounts of memory.
        <!-- Fixed in trunk: r36101 -->
        <!-- Fixed in trunk-1.4: r36102 -->
        <!-- Fixed in trunk-1.2: r36103 -->
        (<a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=5732">Bug
        5732</a>)
      </br>
      Versions affected: 1.2.0 to 1.2.14 and 1.4.0 to 1.4.3. (Prior versions including 1.0.x are also affected.)</br>
      <!--
        <a href="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2011-????">CVE-2011-????</a>
      </br>
      -->
    
    
    </ul>
    </p>
    
    <h2>Impact</h2>
    
    <p>
    It may be possible to make Wireshark crash by injecting a series of malformed
    packets onto the wire or by convincing someone to read a malformed packet trace
    file.
    </p>
    
    <h2>Resolution</h2>
    
    <p>
    Upgrade to Wireshark {{ fixed_versions }} or later.
    Due to the nature of these bugs we do not recommend trying to work around the
    problem by disabling dissectors.
    </p>
    
    
