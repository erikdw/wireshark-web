file_date: February 27, 2008
fixed_versions: 0.99.8

name: |
    Multiple problems in Wireshark (formerly Ethereal)

affected_versions: |
    0.6.0 up to and including 0.99.7

body: |
    <p>
    Wireshark 0.99.8 fixes the following vulnerabilities:
    </p>
    
    <ul>
    
    <li>
      The SCTP dissector could crash.
      <!-- Fixed in r24471 -->
      <br />
      Versions affected: 0.99.5 to 0.99.7
      <!-- <ulink url="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2007-????">CVE-2007-????</ulink> -->
    
    <li>
      The SNMP dissector could crash.
      <!-- Fixed in r23962, r24467 -->
      (Bugs <href url="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=2144" name="2144"> and <href url="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=2277" name="2277">)
      <br />
      Versions affected: 0.99.6 to 0.99.7
      <!-- <ulink url="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2007-????">CVE-2007-????</ulink> -->
    
    <li>
      The TFTP dissector could crash Wireshark on Ubuntu 7.10. (This
      appears to be a bug in the Cairo library on that platform.) Reported
      by Noam Rathaus.
      <!-- Fixed in r24341 -->
      <br />
      Versions affected: 0.6.0 to 0.99.7
      <!-- <ulink url="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2007-????">CVE-2007-????</ulink> -->
    
    
    </ul>
    
    <h2>Impact</h2>
    
    <p>
    It may be possible to make Wireshark or Ethereal crash or use
    up available memory by injecting a purposefully malformed packet
    onto the wire or by convincing someone to read a malformed packet
    trace file.
    </p>
    
    <h2>Resolution</h2>
    
    <p>
    Upgrade to Wireshark 0.99.8.
    </p>
    
    <p>
    If are running Wireshark 0.99.7 or Ethereal 0.99.0 or earlier and
    cannot upgrade, you can work around each of the problems listed above
    by doing the following:
    </p>
    
    <ul class="item-list">
    
      <li>Disable the SCTP, SNMP and TFTP dissectors.
    
        <ul class="item-list">
    
          <li>Select <em>Analyze&#8594;Enabled Protocols...</em> from the menu.
    
          <li>Make sure "SCTP," "SNMP," and "TFTP" are un-checked.
    
          <li>Click "Save", then click "OK".
    
        </ul>
    
    </ul>
    
    
