file_date: April 6, 2009
start_version: 0.9.6
end_version: 1.0.6
fixed_versions: 1.0.7

name: |
    Multiple problems in Wireshark

affected_versions: |
    {{ start_version }} up to and including
    {{ end_version }}

body: |
    <p>
    Wireshark {{ fixed_versions }} fixes the following vulnerabilities:
    </p>
    
    <ul>
    
    <li>
      The PROFINET dissector was vulnerable to a format string overflow.
      <!-- Fixed in trunk: r26652 -->
      <!-- Fixed in trunk-1.0: r27923 -->
      (<a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=3382">Bug
        3382</a>)
      Versions affected: 0.99.6 to 1.0.6
      <a href="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-1210">CVE-2009-1210</a>
    
    <li>
      The LDAP dissector could crash on Windows.
      <!-- Fixed in trunk: r27478, 27482 -->
      <!-- Fixed in trunk-1.0: r27976 -->
      (<a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=3262">Bug
        3262</a>)
      Versions affected: 0.99.2 to 1.0.6
      <a href="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-1267">CVE-2009-1267</a>
    
    <li>
      The Check Point High-Availability Protocol (CPHAP) dissector could crash.
      <!-- Fixed in trunk: r27083 -->
      <!-- Fixed in trunk-1.0: r27934 -->
      (<a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=3269">Bug
        3269</a>)
      Versions affected: 0.9.6 to 1.0.6
      <a href="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-1268">CVE-2009-1268</a>
    
    <li>
      Wireshark could crash while loading a Tektronix .rf5 file.
      <!-- Fixed in trunk: r27918 -->
      <!-- Fixed in trunk-1.0: r27975 -->
      (<a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=3366">Bug
        3366</a>)
      Versions affected: 0.99.6 to 1.0.6
      <ulink url="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-1269">CVE-2009-1269</ulink>
    
    </ul>
    
    <h2>Impact</h2>
    
    <p>
    It may be possible to make Wireshark crash by altering the HOME environment
    variable or by convincing someone to read a malformed packet trace file.
    </p>
    
    <h2>Resolution</h2>
    
    <p>
    Upgrade to Wireshark {{ fixed_versions }} or later.
    </p>
    
    <p>
    If are running Wireshark {{ end_version }} or earlier (including Ethereal 0.99.0) and cannot
    upgrade, you can work around each of the problems listed above by doing the
    following:
    </p>
    
    <ul class="item-list">
    
      <li>For each user that will run Wireshark (including root if you're running
      Wireshark as a privileged user), make sure the HOME environment variable
      doesnt' contain any "%" characters.</li>
      
      <li>Don't open any Tektronix K12 text or NetScreen capture files.</li>
    
    </ul>
    
    
    
