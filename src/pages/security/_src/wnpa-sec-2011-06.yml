file_date: April 15, 2011
start_version: 1.4.0
end_version: 1.4.4
fixed_versions: 1.4.5

name: |
    DECT, NFS, and X.509if vulnerabilities in Wireshark

affected_versions: |
    {{ start_version }} <!-- up to and including
    {{ end_version }} -->

related: |
    <a href="wnpa-sec-2011-05.html">wnpa-sec-2011-05</a>
    (X.509if vulnerability in Wireshark version 1.2.0 to 1.2.14 )

body: |
    <p>
    Wireshark {{ fixed_versions }} fixes the following vulnerabilities:
    </p>
    
    <p>
    <ul>
    
    <li>
        The NFS dissector could crash on Windows.
        <!-- This is due to Visual C++ not supporting C99. Do non-Windows, non-gcc Wireshark packages exist? -->
        <!-- Fixed in trunk: r34115 -->
        <!-- Fixed in trunk-1.4: r36570 -->
        (<a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=5209">Bug
        5209</a>)
        </br>
        Versions affected: 1.4.0 to 1.4.4.
        <!--
        <a href="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2010-????">CVE-2010-????</a>
        -->
    
    <li>
        The X.509if dissector could crash.
        <!-- Fixed in trunk: r36608 -->
        <!-- Fixed in trunk-1.4: r36611 -->
        <!-- Fixed in trunk-1.2: r36612 -->
        (<a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=5754">Bug
        5754</a>,
        <a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=5793">Bug
        5793</a>)
        </br>
        Versions affected: 1.2.0 to 1.2.15 and 1.4.0 to 1.4.4.
        <!--
        <a href="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2010-????">CVE-2010-????</a>
        -->
    
    <li>
        Paul Makowski from SEI/CERT discovered that the DECT dissector could
        overflow a buffer. He verified that this could allow remote code
        execution on many platforms.
        <!-- Fixed in trunk-1.4: r36644 -->
        </br>
        Versions affected: 1.4.0 to 1.4.4.
    
    </ul>
    </p>
    
    <h2>Impact</h2>
    
    <p>
    It may be possible to make Wireshark crash by injecting a series of malformed
    packets onto the wire or by convincing someone to read a malformed packet trace
    file.
    </p>
    
    <h2>Resolution</h2>
    
    <p>
    Upgrade to Wireshark {{ fixed_versions }} or later.
    </p>
    
    <p>
    If are running Wireshark {{ end_version }} or earlier (including Ethereal)
    and cannot upgrade, you can work around the problem listed above by
    doing the following:
    </p>
    
    <ul class="item-list">
    
      <li>Disable the DECT, NFS (if you're running Windows), and X.509if dissectors:
    
        <ul>
    
          <li>Select <em>Analyze&#8594;Enabled Protocols...</em> from the menu.
    
          <li>Make sure "DECT", "NFS", and "X.509if" are un-checked.
    
          <li>Click "Save", then click "OK".
    
        </ul>
    
    </ul>
    
    
    
