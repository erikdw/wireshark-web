file_date: November 18, 2010
start_version: 1.4.0
end_version: 1.4.1
fixed_versions: 1.4.2

name: |
    LDSS and ZigBee ZCL vulnerabilities in Wireshark

affected_versions: |
    {{ start_version }} <!-- up to and including
    {{ end_version }} -->

related: |
    <a href="wnpa-sec-2010-13.html">wnpa-sec-2010-13</a>
    (LDSS vulnerability in Wireshark version 1.2.0 to 1.2.12 )

body: |
    <p>
    Wireshark {{ fixed_versions }} fixes the following vulnerability:
    </p>
    
    <p>
    <ul>
    
    <li>
      Nephi Johnson of BreakingPoint discovered that the LDSS dissector
      could overflow a buffer.
      <!-- Fixed in trunk: r34581 -->
      <!-- Fixed in trunk-1.4: r34889 -->
      <!-- Fixed in trunk-1.2: r34941 -->
      (<a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=5318">Bug
      5318</a>)
      Versions affected: 1.2.0 to 1.2.12 and 1.4.0 to 1.4.1.
      <a href="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2010-4300">CVE-2010-4300</a>
    
    <li>
      The ZigBee ZCL dissector could go into an infinite loop.
      <!-- Fixed in trunk: r34575 -->
      <!-- Fixed in trunk-1.4: r34889 -->
      (<a href="https://bugs.wireshark.org/bugzilla/show_bug.cgi?id=5303">Bug
      5303</a>)
      Versions affected: 1.4.0 to 1.4.1.
      <a href="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2010-4301">CVE-2010-4301</a>
    
    </ul>
    </p>
    
    <h2>Impact</h2>
    
    <p>
    It may be possible to make Wireshark crash by injecting a series of malformed
    packets onto the wire or by convincing someone to read a malformed packet trace
    file.
    </p>
    
    <h2>Resolution</h2>
    
    <p>
    Upgrade to Wireshark {{ fixed_versions }} or later.
    </p>
    
    <p>
    If are running Wireshark {{ end_version }} or earlier (including Ethereal)
    and cannot upgrade, you can work around each of the problems listed above by
    doing the following:
    </p>
    
    <ul class="item-list">
    
      <li>Disable the LDSS and ZigBee ZCL dissectors:
    
        <ul>
    
          <li>Select <em>Analyze&#8594;Enabled Protocols...</em> from the menu.
    
          <li>Make sure "LDSS" and "ZigBee ZCL" are un-checked.
    
          <li>Click "Save", then click "OK".
    
        </ul>
    
    </ul>
    
    
