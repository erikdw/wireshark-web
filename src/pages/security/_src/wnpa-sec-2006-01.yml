file_date: July 17, 2006
fixed_versions: 0.99.2

name: |
    Multiple problems in Ethereal

affected_versions: |
    0.8.16 up to and including 0.99.0

body: |
    <p>
    Wireshark 0.99.2 fixes the following vulnerabilities:
    </p>
    
    <ul class="item-list">
    
    <li>
        The GSM BSSMAP dissector could crash.
        <!-- Fixed in r18586 -->
        <!-- Bug IDs: none -->
        Versions affected: 0.10.11 to 0.99.0.
        CVE: <a href="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2006-3627">CVE-2006-3627</a>
    
    </ul>
    
          Ilja van Sprundel discovered the following vulnerabilities:
    
    <ul class="item-list">
    
    <li>
        The ANSI MAP dissector was vulnerable to a format string overflow.
        <!-- Fixed in r18677 -->
        <!-- Bug IDs: none -->
        Versions affected: 0.10.0 to 0.99.0.
        CVE: <a href="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2006-3628">CVE-2006-3628</a>
    
    <li>
        The Checkpoint FW-1 dissector was vulnerable to a format string
              overflow.
        <!-- Fixed in r18677 -->
        <!-- Bug IDs: none -->
        Versions affected: 0.10.10 to 0.99.0.
        CVE: <a href="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2006-3628">CVE-2006-3628</a>
    
    <li>
        The MQ dissector was vulnerable to a format string overflow.
        <!-- Fixed in r18677 -->
        <!-- Bug IDs: none -->
        Versions affected: 0.10.4 to 0.99.0.
        CVE: <a href="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2006-3628">CVE-2006-3628</a>
    
    <li>
        The XML dissector was vulnerable to a format string overflow.
        <!-- Fixed in r18677 -->
        <!-- Bug IDs: none -->
        Versions affected: 0.10.13 to 0.99.0.
        CVE: <a href="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2006-3628">CVE-2006-3628</a>
    
    <li>
        The MOUNT dissector could attempt to allocate large amounts of
        memory.
        <!-- Fixed in r18678, r18684, r18693 -->
        <!-- Bug IDs: none -->
        Versions affected: 0.9.4 to 0.99.0.
        CVE: <a href="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2006-3629">CVE-2006-3629</a>
    
    <li>
        The NCP NMAS and NDPS dissectors were susceptible to
        off-by-one errors.
        <!-- Fixed in r18678, r18710 -->
        <!-- Bug IDs: none -->
        Versions affected: 0.9.7 to 0.99.0.
        CVE: <a href="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2006-3630">CVE-2006-3630</a>
    
    <li>
        The NTP dissector was vulnerable to a format string overflow.
        <!-- Fixed in r18678 -->
        <!-- Bug IDs: none -->
        Versions affected: 0.10.13 to 0.99.0.
        CVE: <a href="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2006-3628">CVE-2006-3628</a>
    
    <li>
        The SSH dissector was vulnerable to an infinite loop.
        <!-- Fixed in r18692, r18693 -->
        <!-- Bug IDs: none -->
        Versions affected: 0.9.10 to 0.99.0.
        CVE: <a href="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2006-3631">CVE-2006-3631</a>
    
    <li>
        The NFS dissector may have been susceptible to a buffer overflow.
        <!-- Fixed in r18699 -->
        <!-- Bug IDs: none -->
        Versions affected: 0.8.16 to 0.99.0.
        CVE: <a href="http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2006-3632">CVE-2006-3632</a>
    
    </ul>
    
    <h2>Impact</h2>
    
    <p>
    It may be possible to make Ethereal crash, use up available
    memory, or run arbitrary code by injecting a purposefully
    malformed packet onto the wire or by convincing someone to read
    a malformed packet trace file.
    </p>
    
    <h2>Resolution</h2>
    
    <p>
    Upgrade to Wireshark 0.99.2.
    </p>
    
    <p>
    If are running Ethereal 0.99.0 or earlier and cannot upgrade, you can disable
    each of the dissectors listed above by doing the following:
    </p>
    
    <ul class="item-list">
    
      <li>Select <em>Analyze&#8594;Enabled Protocols...</em> from the menu.
    
      <li>Make sure <strong>every</strong> protocol listed above is un-checked.
    
      <li>Click "Save", then click "OK".
    
    </ul>
    
    
