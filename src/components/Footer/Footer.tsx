import styles from "./Footer.module.scss";
import links from "../../utils/links";

const Footer = () => {
  const renderGetHelpSection = links.getHelp.subLinks.map((section) => (
    <li>
      <a href={section.url}>{section.name}</a>
    </li>
  ));

  return (
    <section id={styles.footer}>
      <div className={styles.leftSide}>
        <img
          alt="Wireshark logo footer"
          src="/assets/img/wireshark-logo.png"
          height="40px"
          width="150px"
        />
        <p>
          © <a href={links.foundation.url}>{links.foundation.name} <span class="separator">·</span> <a href={links.privacyPolicy.url}>{links.privacyPolicy.name}</a></a>
        </p>
        <span>
          <a href={links.wiresharkNewsFacebook.url}>Facebook</a>
          <span class="separator">·</span>
          <a href={links.wiresharkIOCExchange.url}>Mastodon</a>
          <span class="separator">·</span>
          <a href={links.wiresharkNewsTwitter.url}>Twitter</a>
          <span class="separator">·</span>
          <a href={links.sharkFestYouTube.url}>YouTube</a>
        </span>
      </div>
      <div className={styles.center}>
        <ul>
          <li>Get Wireshark</li>
          <li>
            <a href={links.download.url}>{links.download.name}</a>
          </li>
          <li>
            <a href="/code-of-conduct.html">Code of Conduct</a>
          </li>
        </ul>
        <ul>
          <li>{links.getHelp.name}</li>
          {renderGetHelpSection}
        </ul>
      </div>
      <div className={styles.rightSide}>
        <ul>
          <li>Contribute</li>
          {/* <li>
            <a href="#">Get Involved</a>
          </li> */}
          <li>
            <a href="/docs/wsdg_html_chunked/">Developer's Guide</a>
          </li>
          <li>
            <a href="https://gitlab.com/wireshark/wireshark/-/tree/master">
              Browse the Code
            </a>
          </li>
          <li>
            <a href="/about.html#authors">Authors</a>
          </li>
          <li>
            <a href={links.members.url}>{links.members.name}</a>
          </li>
          <li>
            <a href={links.donate.url}>Donate</a>
          </li>
        </ul>
        <ul>
          <li>Learn</li>
          <li>
            <a href={links.getAcquainted.subLinks[0].url}>
              {links.getAcquainted.subLinks[0].name}
            </a>
          </li>
          <li>
            <a href={links.news.url}>{links.news.name}</a>
          </li>
          <li>
            <a href={links.sharkFest.url}>{links.sharkFest.name}</a>
          </li>
          <li>
            <a href={links.sharkFestYouTube.url}>
              {links.sharkFestYouTube.name}
            </a>
          </li>
          <li>
            <a href="https://blog.wireshark.org">Blog</a>
          </li>
        </ul>
      </div>
    </section>
  );
};

export default Footer;
