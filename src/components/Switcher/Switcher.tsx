import { useEffect, useState } from "preact/hooks";
import styles from "./Switcher.module.scss"
import { THEME_OPTIONS } from "../../utils/types";

interface SwitcherProps {
  isLightMode: boolean;
  setIsLightMode: (isLightMode: boolean) => void;
}

const Switcher: preact.FunctionComponent<SwitcherProps> = ({ isLightMode, setIsLightMode }) => {

  return (
    <div className={styles.switchWrapper}>
      {isLightMode && <img onClick={() => setIsLightMode(false)} className={styles.sun} src="/assets/icons/phosphor/moon.svg" />}
      {!isLightMode && <img onClick={() => setIsLightMode(true)} className={styles.moon} src="/assets/icons/phosphor/sun.svg" />}
    </div>
  )
}

export default Switcher;
