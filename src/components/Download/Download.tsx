import DownloadAccordion from "./DownloadAccordion";
import DonationSection from "../Home/DonationSection";
import SponsorBanners from "../SponsorBanners/SponsorBanners";
import styles from "./Download.module.scss";
import TextBar from "../TextBar/TextBar";
import versions from "./Versions.json";

const Download: preact.FunctionComponent = () => {
  return (
    <section id={styles.downloadPage}>
      <div>
        <h1>Download Wireshark</h1>
        <p>
          The current stable release of Wireshark is {versions.stable}. It
          supersedes all previous releases.
          {versions.development != null && (
            <span>
              You can also download the latest development release (
              {versions.development}) and documentation.
            </span>
          )}
        </p>
        <DownloadAccordion />
        <SponsorBanners />
      </div>
      <DonationSection />
      <div>
        <TextBar text="Not What You're Looking For?" />
        <h3>Older Releases</h3>
        <p>
          All present and past releases can be found in our{" "}
          <a href="#spelunking">our download area</a>.
        </p>
        <h3>Installation Notes</h3>
        <p>
          For a complete list of system requirements and supported platforms,
          please consult{" "}
          <a href="/docs/wsug_html_chunked/ChIntroPlatforms.html">
            the User's Guide
          </a>
          .
        </p>
        <p>
          Information about each release can be found in{" "}
          <a href="/docs/relnotes/">the release notes</a>.
        </p>
        <p>
          Each Windows package comes with the latest stable release of Npcap,
          which is required for live packet capture. If needed you can download
          separately from the <a href="https://npcap.com/">Npcap</a> web site.
        </p>
        {/* <p>
          You can also capture packets using{" "}
          <a href="https://www.winpcap.org/">
            WinPcap
          </a>
          , although it is no longer maintained or supported.
        </p> */}
        <h3>Live on the Bleeding Edge</h3>
        <p>
          You can download source code packages and Windows installers which are
          automatically created each time code is checked into the{" "}
          <a href="https://gitlab.com/wireshark/wireshark/">
            source code repository
          </a>
          . These packages are available in the{" "}
          <a href="/download/automated/">automated build section</a> of our
          download area.
        </p>
        <h3 id="spelunking">Go Spelunking</h3>
        <p>
          You can explore the download areas of the main site and mirrors below.
          Past releases can be found by browsing the <i>all-versions</i>{" "}
          directories under each platform directory.
        </p>
        <div id="dl_mirrors">
          <ul>
            <li>
              <a href="https://1.na.dl.wireshark.org">
                Wireshark Foundation (https, us)
              </a>
            </li>
            <li>
              <a href="https://2.na.dl.wireshark.org">
                Wireshark Foundation (https, us)
              </a>
            </li>
            <li>
              <a href="https://1.eu.dl.wireshark.org">
                Wireshark Foundation (https, nl)
              </a>
            </li>
            <li>
              <a href="https://1.as.dl.wireshark.org">
                Wireshark Foundation (https, singapore)
              </a>
            </li>
            <li>
              <a href="ftp://ftp.uni-kl.de/pub/wireshark">
                University of Kaiserslautern (ftp, de)
              </a>
            </li>
            <li>
              <a href="http://ftp.uni-kl.de/pub/wireshark/">
                University of Kaiserslautern (http, de)
              </a>
            </li>
            <li>
              <a href="http://ftp.yz.yamagata-u.ac.jp/pub/network/security/wireshark/">
                Yamagata University, Japan (http, jp)
              </a>
            </li>
            <li>
              <a href="ftp://ftp.yz.yamagata-u.ac.jp/pub/network/security/wireshark/">
                Yamagata University, Japan (ftp, jp)
              </a>
            </li>
            <li>
              <a href="rsync://ftp.yz.yamagata-u.ac.jp/pub/network/security/wireshark/">
                Yamagata University, Japan (rsync, jp)
              </a>
            </li>
            <li>
              <a href="https://wireshark.marwan.ma/download/">
                MARWAN, Morocco (https, ma)
              </a>
            </li>
            <li>
              <a href="https://www.wireshark.org/download">
                Wireshark.org (https, us)
              </a>
            </li>
          </ul>
        </div>

        <h3>Stay Current</h3>

        <p>
          You can stay informed about new Wireshark releases by subscribing to
          the <a href="/lists">wireshark-announce mailing list</a>. We also
          provide a <a href="xml/wireshark-pad.xml">PAD file</a> to make
          automated checking easier.
        </p>

        <h3>Verify Downloads</h3>

        <p>
          File hashes for the current release can be found in the{" "}
          <a href={"/download/SIGNATURES-" + versions.stable + ".txt"}>
            signatures file
          </a>
          . It is signed with{" "}
          <a href="/download/gerald_at_wireshark_dot_org.gpg">
            key id 0xE6FEAEEA
          </a>
          . Prior to April 2016 downloads were signed with{" "}
          <a href="/download/gpg_transition_statement_2016.txt">
            key id 0x21F2949A
          </a>
          .
        </p>
        <h3>Stay Legal</h3>
        <p>
          Wireshark is subject to
          <a href="/export.html"> U.S. export regulations</a>. Take heed.
          Consult a lawyer if you have any questions.
        </p>
      </div>
      <div>
        <TextBar text="Third-Party Packages" />
        <p>
          Wireshark packages are available for most platforms, including the
          ones listed below.
        </p>
        <p>
          <strong>Standard package</strong>: Wireshark is available via the
          default packaging system on that platform.
        </p>
        <table>
          <tbody>
            <tr>
              <th>Vendor / Platform</th>
              <th>Sources</th>
            </tr>

            <tr>
              <td>Alpine / Alpine Linux</td>
              <td>
                <a href="https://pkgs.alpinelinux.org/packages?name=wireshark">
                  Standard package
                </a>
              </td>
            </tr>
            <tr>
              <td>Apple / macOS</td>
              <td>
                <a href="https://formulae.brew.sh/cask/wireshark">
                  Homebrew cask (includes UI)
                </a>{" "}
                <a href="https://formulae.brew.sh/formula/wireshark">
                  Homebrew formula (CLI only)
                </a>
                <a href="https://www.macports.org/">MacPorts</a>
                <a href="http://pdb.finkproject.org/pdb/package.php/wireshark">
                  Fink
                </a>
              </td>
            </tr>
            <tr>
              <td>Arch Linux / Arch Linux</td>
              <td>
                <a href="https://www.archlinux.org/packages/?sort=&q=wireshark">
                  Standard package
                </a>
              </td>
            </tr>
            <tr>
              <td>Canonical / Ubuntu</td>
              <td>
                <a href="https://launchpad.net/ubuntu/+source/wireshark">
                  Standard package
                </a>
                <a href="https://launchpad.net/~wireshark-dev/+archive/ubuntu/stable">
                  Latest stable PPA
                </a>
              </td>
            </tr>
            <tr>
              <td>Debian / Debian GNU/Linux</td>
              <td>
                <a href="https://packages.qa.debian.org/w/wireshark.html">
                  Standard package
                </a>
              </td>
            </tr>
            <tr>
              <td>The FreeBSD Project / FreeBSD</td>
              <td>
                <a href="https://www.freshports.org/net/wireshark/">
                  Standard package
                </a>
              </td>
            </tr>
            <tr>
              <td>Gentoo Foundation / Gentoo Linux</td>
              <td>
                <a href="https://packages.gentoo.org/package/net-analyzer/wireshark">
                  Standard package
                </a>
              </td>
            </tr>
            <tr>
              <td>HP / HP-UX</td>
              <td>
                <a href="http://hpux.connect.org.uk/">
                  Porting And Archive Centre for HP-UX
                </a>
              </td>
            </tr>
            <tr>
              <td>NetBSD Foundation / NetBSD</td>
              <td>
                <a href="ftp://ftp.netbsd.org/pub/pkgsrc/current/pkgsrc/net/wireshark/">
                  Standard package
                </a>
              </td>
            </tr>
            <tr>
              <td>NixOS / NixOS</td>
              <td>
                <a href="https://search.nixos.org/packages?query=wireshark&channel=unstable">
                  Standard package
                </a>
              </td>
            </tr>
            <tr>
              <td>openSUSE / openSUSE</td> <td>Standard package</td>
            </tr>
            <tr>
              <td>Offensive Security / Kali Linux</td>
              <td>
                <a href="https://pkg.kali.org/pkg/wireshark">
                  Standard package
                </a>
              </td>
            </tr>
            <tr>
              <td>OpenPKG / OpenPKG Project</td>
              <td>
                <a href="http://www.openpkg.org/product/packages/?package=wireshark">
                  Standard package
                </a>
              </td>
            </tr>

            <tr>
              <td>PC-BSD Software &#183; iXsystems / PC-BSD</td>
              <td>
                <a href="http://pbidir.com/bt/pbi/67/wireshark">
                  Push Button Installer
                </a>
              </td>
            </tr>
            <tr>
              <td>PCLinuxOS / PCLinuxOS</td> <td>Standard package</td>
            </tr>

            <tr>
              <td>Red Hat / Fedora</td>
              <td>
                <a href="https://packages.fedoraproject.org/pkgs/wireshark/wireshark/">
                  Standard package
                </a>
              </td>
            </tr>
            <tr>
              <td>Red Hat / Red Hat Enterprise Linux</td>{" "}
              <td>Standard package</td>
            </tr>
            <tr>
              <td>Slackware Linux / Slackware</td>
              <td>
                <a href="https://slackbuilds.org/">SlackBuilds.org</a>
              </td>
            </tr>
            <tr>
              <td>Oracle / Solaris 11</td>
              <td>
                <a href="http://pkg.oracle.com/solaris/release/en/search.shtml?token=wireshark&action=Search">
                  Standard package
                </a>
                <a href="https://www.opencsw.org/">CSW</a>
                <a href="https://unixpackages.com/">UNIX Packages</a>
              </td>
            </tr>
            <tr>
              <td>* / *</td>
              <td>
                <a href="http://www.thewrittenword.com/">The Written Word</a>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </section>
  );
};

export default Download;
