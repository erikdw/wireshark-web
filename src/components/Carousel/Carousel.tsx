import styles from "./Carousel.module.scss";
import { SponsorLink } from "../../utils/types";

interface CarouselProps {
  data: SponsorLink[];
}

const Carousel: preact.FunctionComponent<CarouselProps> = ({ data }) => {
  const renderCarouselImages = data.map((item) => {
    return <img src={item.imgPath} />;
  });

  return (
    <section className={styles.carouselContainer}>
      <div className={styles.scrollParent}>
        <div className={`${styles.scrollElement} ${styles.primary}`}>
          {renderCarouselImages}
        </div>
        <div className={`${styles.scrollElement} ${styles.secondary}`}>
          {renderCarouselImages}
        </div>
      </div>
    </section>
  );
};

export default Carousel;
