import styles from "./SponsorBanners.module.scss";

const SponsorBanners: preact.FunctionComponent = () => {
  return (
    <div className={styles.sponsorCardWrapper}>
      <div class="">
        <a target="_blank" href="https://www.endace.com/endaceprobe?utm_source=wireshark&utm_medium=banner&utm_campaign=dl-page">
          <img
            class="banner-img-downloads"
            src="/assets/img/sponsors/banners/endace.png"
            alt="Endace"
          />
        </a>
      </div>
      <div class="">
        <a target="_blank" href="https://www.liveaction.com">
          <img
            class="banner-img-downloads"
            src="/assets/img/sponsors/banners/liveaction.jpg"
            alt="LiveAction"
          />
        </a>
      </div>
      <div class="">
        <a target="_blank" href="https://www.comworth.com.sg/swiftwing/?from=wireshark">
          <img
            class="banner-img-downloads"
            src="/assets/img/sponsors/banners/comworth.png"
            alt="Comworth"
          />
        </a>
      </div>
      <div class="">
        <a target="_blank" href="https://fmad.io/001bbf9ab910b0556e4b666b370abffb.html">
          <img
            class="banner-img-downloads"
            src="/assets/img/sponsors/banners/fmadio.png"
            alt="Fmadio"
          />
        </a>
      </div>
      <div class="">
        <a target="_blank" href="https://www.scos.training/wireshark-training/troubleshooting-tcp-ip-networks/">
          <img
            class="banner-img-downloads"
            src="/assets/img/sponsors/banners/scos-ad.jpg"
            alt="SCOS"
          />
        </a>
      </div>
    </div>
  );
};

export default SponsorBanners;
