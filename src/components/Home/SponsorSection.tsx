import styles from "./Homepage.module.scss";
import { sponsorLinks } from '../../utils/links';
import Carousel from "../Carousel/Carousel";

const SponsorSection: preact.FunctionComponent = () => {
  return (
    <section id={styles.sponsors}>
      <div className={styles.sponsorWrapper}>
        <Carousel data={sponsorLinks} />
        <h3>
          <a className="aDark" href="/members">
            Meet our members
          </a>{" "}
        </h3>
      </div>
    </section>
  );
};

export default SponsorSection;
