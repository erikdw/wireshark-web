
import styles from "./Homepage.module.scss";
import DownloadAccordion from "../Download/DownloadAccordion";
import SponsorBanners from "../SponsorBanners/SponsorBanners";

const DownloadSection: preact.FunctionComponent = () => {
  return (
    <section id={styles.download}>
      <h1>Download Wireshark</h1>
      <div className={styles.downloadWrapper}>
        <DownloadAccordion />
        <SponsorBanners />
      </div>
      <p>
        More downloads and documentation can be found on the{" "}
        <a href="/download.html">downloads page</a>.
      </p>
    </section>
  );
};

export default DownloadSection;
