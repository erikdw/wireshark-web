import styles from "./AboutPage.module.scss";

const AboutPage: preact.FunctionComponent = () => {
  return (
    <section id={styles.aboutPage}>
      <h1>About Wireshark</h1>
      <p>
        Wireshark is the world's foremost network protocol analyzer. It lets you
        see what's happening on your network at a microscopic level. It is the
        de facto (and often de jure) standard across many industries and
        educational institutions.{" "}
      </p>
      <p>
        Wireshark development thrives thanks to the contributions of networking
        experts across the globe. It is the continuation of a project that
        started in 1998.
      </p>
      <p>
        You can follow us on Twitter at{" "}
        <a href="https://twitter.com/WiresharkNews">@WiresharkNews</a>.
      </p>
      <h2>Awards and Accolades</h2>
      <ul>
        <li>
          <a href="https://www.hostingadvice.com/blog/the-wireshark-network-and-packet-analysis-tool-turns-20/">
            HostingAdvice - Wireshark Turns 20
          </a>
        </li>
        <li>
          {/* <a href="http://www.infoworld.com/slideshow/2008/08/170-best_of_open_so-8.html"> */}
          <a href="https://web.archive.org/web/20081220223304/http://www.infoworld.com/slideshow/2008/08/170-best_of_open_so-8.html">
            InfoWorld BOSSIE 2008 - VoIP Monitoring
          </a>
        </li>
        {/* Dead link */}
        {/* <li>
          <a href="http://www.infoworld.com/archives/t.jsp?N=s&amp;V=91650">
            InfoWorld BOSSIE 2007 - Network Protocol Analysis
          </a>
        </li> */}
        <li>
          <a href="http://www.eweek.com/c/a/Linux-and-Open-Source/The-Most-Important-OpenSource-Apps-of-All-Time/5/">
            eWeek &#183; The Most Important Open-Source Apps of All Time
          </a>
        </li>
        <li>
          <a href="http://www.pcmag.com/article2/0,2817,2360043,00.asp">
            PC Magazine &#183; Editor's Choice
          </a>
        </li>
        <li>
          <a href="https://www.siteadvisor.com/sites/wireshark.org">
            McAfee SiteAdvisor
          </a>
        </li>
        <li>
          <a href="http://sectools.org/">Insecure.Org / Sectools.Org</a>
        </li>
      </ul>
      <h2>Features</h2>
      <ul class="item-list">
        <li>
          <a href="/docs/dfref/">Deep inspection of hundreds of protocols</a>,
          with more being added all the time
        </li>
        <li>Live capture and offline analysis</li>
        <li>Standard three-pane packet browser</li>
        <li>
          Multi-platform: Runs on Windows, Linux, OS X, FreeBSD, NetBSD, and
          many others
        </li>
        <li>
          Captured network data can be browsed via a GUI, or via the TTY-mode
          TShark utility
        </li>
        <li>The most powerful display filters in the industry</li>
        <li>Rich VoIP analysis</li>
        <li>
          Read/write many different capture file formats: tcpdump (libpcap),
          Pcap NG, Catapult DCT2000, Cisco Secure IDS iplog, Microsoft Network
          Monitor, Network General Sniffer&#174; (compressed and uncompressed),
          Sniffer&#174; Pro, and NetXray&#174;, Network Instruments Observer,
          NetScreen snoop, Novell LANalyzer, RADCOM WAN/LAN Analyzer,
          Shomiti/Finisar Surveyor, Tektronix K12xx, Visual Networks Visual
          UpTime, WildPackets EtherPeek/TokenPeek/AiroPeek, and many others
        </li>
        <li>
          Capture files compressed with gzip can be decompressed on the fly
        </li>
        <li>
          Live data can be read from Ethernet, IEEE 802.11, PPP/HDLC, ATM,
          Bluetooth, USB, Token Ring, Frame Relay, FDDI, and others (depending
          on your platform)
        </li>
        <li>
          Decryption support for many protocols, including IPsec, ISAKMP,
          Kerberos, SNMPv3, SSL/TLS, WEP, and WPA/WPA2
        </li>
        <li>
          Coloring rules can be applied to the packet list for quick, intuitive
          analysis
        </li>
        <li>
          Output can be exported to XML, PostScript&#174;, CSV, or plain text
        </li>
      </ul>
      <h2 id="authors">Authors</h2>
    </section>
  );
};

export default AboutPage;
