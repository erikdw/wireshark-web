import { FunctionComponent } from "preact";
import { useEffect, useState } from "preact/hooks";
import Header from "../Header/Header";
import Footer from "../Footer/Footer";
import { THEME_OPTIONS } from "../../utils/types";

const AppWrapper: FunctionComponent = ({ children }) => {
  const [isLightMode, setIsLightMode] = useState(localStorage.getItem("theme") === THEME_OPTIONS.LIGHT);

  useEffect(() => {
    if (isLightMode) {
      localStorage.setItem("theme", THEME_OPTIONS.LIGHT);
      document.documentElement.className = THEME_OPTIONS.LIGHT
    } else {
      localStorage.setItem("theme", THEME_OPTIONS.DARK)
      document.documentElement.className = THEME_OPTIONS.DARK
    }
  }, [isLightMode])

  return (
    <>
      <Header isLightMode={isLightMode} setIsLightMode={setIsLightMode} />
      {children}
      <Footer />
    </>
  );
};

export default AppWrapper;
